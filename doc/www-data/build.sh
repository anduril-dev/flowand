#!/bin/bash

echo "***** Updating home page images..."
montage \
  front-clust.png  front-mmclustering.png  front-transform.png \
  -tile 3x1 -geometry 400x400+0+0 \
  ../www/demoMontage.png
for pdf in $PDFS; do
    rm $pdf.png
done
