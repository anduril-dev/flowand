
library(componentSkeleton)

execute <- function(cf){

  # Read input
  input.data <- get.input(cf,'clusterFiles')
  reference <- get.input(cf,'reference')

  # Read parameters

  summary.type <- get.parameter(cf, 'summaryType')
  cluster.col <- get.parameter(cf, 'clusterCol')
  clust.id.col <- get.parameter(cf,'culsterIDCol')
  ref.id.col <- get.parameter(cf,'referenceIDCol')
  # Create output
  output.summaries <- get.output(cf,'summaries')
  dir.create(output.summaries)

  #Fetching file names
  input.files=sort(dir(input.data,pattern='.csv',full.names=T))
  reference.files=sort(dir(reference,pattern='.csv',full.names=T))

  # Type checking

  if (length(which(c('mean','median','var')==summary.type))==0){
  	write.error(cf,paste('The Summary type ',summary.type,' is not available.', sep=''))

 	return(PARAMETER_ERROR)
  }


  #Evaluating and calculating the Summaries

  for(input.index in 1:length(input.files)){
  	print(gsub('.*/','',input.files[input.index]))
	input.temp=CSV.read(input.files[input.index])
	reference.temp=CSV.read(reference.files[input.index])

	#Check the validity of the file formats 
	if(length(which(names(input.temp)==clust.id.col))==0){
	  write.error(cf, paste('Error in file', input.files[input.index], 
	  ': Column "',clust.id.col,'" does not exist', sep='' ))
	  return(PARAMETER_ERROR)
	}

	if(length(which(names(reference.temp)==ref.id.col))==0){

	  write.error(cf, paste('Error in file', reference.files[input.index], 
	  ': Column "',ref.id.col,'" does not exist', sep='' ))
	  return(PARAMETER_ERROR)

	}
	
	if(length(which(names(input.temp)==cluster.col))==0){

	  write.error(cf, paste('Error in file', input.files[input.index], 
	  ': Column "',cluster.col,'" does not exist', sep='' ))
	  return(PARAMETER_ERROR)	

	}

	temp.res = eval(parse(text=paste(
	'apply(reference.temp[input.temp[,clust.id.col],-which(names(reference.temp)==ref.id.col)],2,tapply,input.temp[,cluster.col],',
	summary.type,')',sep='')))
	temp.res=as.data.frame(temp.res)
	temp.res[,cluster.col]=sort(unique(input.temp[,cluster.col]),decreasing=F)
	CSV.write(paste(output.summaries,
	gsub('.*/','',input.files[input.index]), sep='/'),
	temp.res)
  }


}

main(execute)
