library(componentSkeleton)
library(flowMeans)

## Width of a sub figure in the report latex document.
WIDTH.OF.SUB.FIGURE = '0.3\\textwidth'

execute <- function(cf) {
    # Read inputs.
    input.csv.list <- get.input(cf,'csvList')
    if (input.defined(cf, 'csv')) {
        csv.array <- Array.read(cf, 'csv')
    }else {
        csv.array <- NULL
    }

    # Get parameters and parse them.
    channels.to.cluster <- get.parameter(cf, 'channelsToCluster')
    if(channels.to.cluster == "*")  {
        channels.to.cluster <- NULL
    }else {
        channels.to.cluster <- split.trim(channels.to.cluster, ",")
    }    
    max.n <- get.parameter(cf, 'maxN', type='int')
    if(max.n < 0) max.n <- NA
    num.c <- get.parameter(cf, 'numC', type='int')
    if(num.c < 0) num.c <- NA
    iter.max <- get.parameter(cf, 'iterMax', type='int')
    if(iter.max < 0) write.error(cf, 'Invalid parameter value.')    
    n.start <- get.parameter(cf, 'nStart', type='int')
    if(n.start < 0) {
        write.error(cf, 'Invalid parameter value.')
    }
    mahalanobis <- get.parameter(cf, 'mahalanobis', type='boolean')
    standardize <- get.parameter(cf, 'standardize', type='boolean')
    cluster.id.col.name <- get.parameter(cf, 'clusterIDColName')
    
    # Component name in Anduril network
    instance.name <- get.metadata(cf, 'instanceName')

    # Get output directory paths and create them.
    output.clusters.list <- get.output(cf, 'clustersList')
    dir.create(output.clusters.list)
    output.clusters <- get.output(cf, 'clusters')
    dir.create(output.clusters)
    output.report <- get.output(cf, 'report')
    dir.create(output.report)

    ## Create list of files to cluster.
    data.files <- list.files(input.csv.list, ".csv$", full.names=T)
    if(!is.null(csv.array)) {
        data.files <- c(data.files, Array.getFiles(csv.array))
    }
    ## List of figures of the optimal clustering finding.
    report.images <- list()

    ## Output array.
    clusters.array <- Array.new()

    for(i in 1:length(data.files)) {
        # Take the filename without the ".fcs" suffix as the sample name.
        temp <- unlist(strsplit(data.files[i], "/"))
        sample.file.name <- temp[length(temp)]        
        sample.name <- sub("\\.csv", "", sample.file.name)
        write.log(cf, sprintf("Clustering file: %s", sample.file.name))
        input.file.name <- data.files[i]
        output.file.name <- file.path(output.clusters.list, sample.file.name)
        output.report.figure.name <- sprintf("%s-%s.pdf", instance.name, sample.name)
        # Do the clustering.
        input.to.clustering <- list(input.file.name=input.file.name, channels.to.cluster=channels.to.cluster, max.n=max.n, num.c=num.c, iter.max=iter.max, n.start=n.start, mahalanobis=mahalanobis, standardize=standardize, output.file.name=output.file.name, output.report=output.report, cluster.id.col.name=cluster.id.col.name, figure.name=output.report.figure.name)
        cluster.flow.means(cf, input.to.clustering)
        report.images[[sample.name]] <- output.report.figure.name
        clusters.array <- Array.add(clusters.array, sample.name, output.file.name)
    }

    ## Write array output.
    Array.write(cf, clusters.array, 'clusters')
    
    ## Construct the LaTex report.
    tex <- character()
    tex <- c(tex, '\\begin{figure}[!ht]\\centering')
    fig.caption <- "Selection of optimal number of clusters for samples:"
    
    for(sample.name in names(report.images)) {
        ## Subfigure caption
        sub.caption <- sample.name
        ## Subfigure label
        sub.label <- sprintf("fig:%s-%s", instance.name, sample.name)
        tex <- c(tex, sprintf('\\subfloat[][]{\\includegraphics[width=%s,keepaspectratio=true]{%s}\\label{%s}}', WIDTH.OF.SUB.FIGURE, report.images[[sample.name]], sub.label))
        tex <- c(tex, '\\hspace{8pt}')
        fig.caption <- paste(fig.caption, sprintf("\\subref{%s} %s", sub.label, latex.quote(sub.caption)))
    }
    
    fig.caption <- paste(fig.caption, ".", sep="")
    tex <- c(tex, sprintf('\\caption{%s}\\label{fig:%s}', fig.caption, instance.name), '\\end{figure}')
    latex.write.main(cf, 'report', tex)
}


# Clustering with FlowMeans.
cluster.flow.means <- function(cf, input) {
    ## Cluster the data.
    csv <- CSV.read(input$input.file.name)
    if(input$cluster.id.col.name %in% colnames(csv)) {
        write.error(cf, sprintf('clusterIDColName %s is already present in the input file %s.', input$cluster.id.col.name, input$input.file.name))
    }    
    col.names <- input$channels.to.cluster[input$channels.to.cluster %in% colnames(csv)]    
    clust.res <- flowMeans(csv, varNames=col.names, MaxN=input$max.n, NumC=input$num.c, iter.max=input$iter.max, nstart=input$n.start, Mahalanobis=input$mahalanobis)
    res.csv <- csv
    res.csv[,input$cluster.id.col.name] <- clust.res@Label
    CSV.write(input$output.file.name, res.csv)

    ## Plot change point detection.
    temp <- unlist(strsplit(input$input.file.name, "/"))
    temp <- temp[length(temp)]
    sample.name <- sub("\\.csv", "", temp)
    pdf(file=file.path(input$output.report, input$figure.name), width=5, height=5)
    plot(clust.res@Mins, xlab = " ", ylab = " ", xlim = c(1, clust.res@MaxN), ylim = c(0, max(clust.res@Mins)))
    ft <- changepointDetection(clust.res@Mins)
    ## If the slope of the fitted lines is NA, it's interpreted as slope being 0. This will be plotted unlike if the slope is NA.
    if(is.na(ft$l1$coefficients[2])) ft$l1$coefficients[2] <- 0
    if(is.na(ft$l2$coefficients[2])) ft$l2$coefficients[2] <- 0
    abline(ft$l1)
    abline(ft$l2)
    par(new = TRUE)
    plot(ft$MinIndex + 1, clust.res@Mins[ft$MinIndex + 1], col = "red", xlab = "Iteration", ylab = "Distance", xlim = c(1, clust.res@MaxN), ylim = c(0, max(clust.res@Mins)), pch = 19, main=sample.name)
    dev.off()
}

main(execute)
