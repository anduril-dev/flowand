<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>PairWiseClusterPlot</name>
    <version>1.0</version>
    <doc>
    Produces pairwise scatter plots from clustered and filtered data.
    The points not present in filtered data will be plotted in grey colour.
    </doc>
    <author email="ali.oghabian@helsinki.fi">Ali Oghabian</author>
    <author email="erkka.valo@helsinki.fi">Erkka Valo</author>
    <category>Plot</category>
    <category>FlowCytometry</category>
    <launcher type="R">
        <argument name="file" value="PairWiseClusterPlot.r" />
    </launcher>
    <requires>R</requires>
    <inputs>
        <input name="clusterFiles" type="CSVList" optional="true">
            <doc> CSV files with clusterd rows to plot.</doc>
        </input>
        <input name="clusters" type="CSV" array="true" optional="true">
            <doc>CSV files with clusterd rows to plot. </doc>
        </input>
        
        <input name="filteredFiles" type="CSVList" optional="true">
            <doc>Filtered CSV files with clusterd rows. The elements in the array
                 should have the same keys as in the <code>clusteredFiles</code> input.
                 A element should contain a subset of rows of the corresponding
                 element in <code>clusteredFiles</code>. The rows not present in 
                 the filtered data will be plotted in grey.</doc> 
        </input>
        <input name="filtered" type="CSV" array="true" optional="true">
            <doc>Filtered CSV files with clusterd rows. The elements in the array
                 should have the same keys as in the <code>clusters</code> input.
                 A element should contain a subset of rows of the corresponding
                 element in <code>clusters</code>. The rows not present in the filtered
                 data will be plotted in grey.
            </doc>
        </input>
    </inputs>
    <outputs>
	    <output name="report" type="Latex">
            <doc>The pairwise scatter plots in a LaTeX document. 
	        </doc>
	    </output>
    </outputs>
    <parameters>
	    <parameter name="pagebreak" type="boolean" default="false">
            <doc>Should the result document start with a page break.</doc>
        </parameter>
	    <parameter name="plotType" type="string" default="regular2d">
		    <doc>The type of the plot. The choices are regular2d, and regular3d.</doc>
	    </parameter>
	    <parameter name="clusterClustIDCol" type="string" default="cluster">
    		<doc> The name of the column in the clusterFiles/clusters which contains 
                  the cluster IDs to which the rows are assigned.  </doc>
		</parameter>
	    <parameter name="clusterIDCol" type="string" default="Event">
    		<doc> The name of the column in the clusterFiles/clusters which represents 
                  the IDs of the rows. </doc>
		</parameter>
	    <parameter name="channelsToPlot" type="string">
    		<doc> The names of the columns to be plotted as a comma-separated 
                  list.</doc>
		</parameter>		
	    <parameter name="filteredIDCol" type="string" default="Event">
    		<doc> The name of the column in the filteredFiles/filtered which represents 
                  the IDs of the rows.</doc>
		</parameter>				
	    <parameter name="pch" type="string" default=".">
		    <doc>The type and shape of plotting points. Numbers (e.g. "2") and
                 characters (e.g. ".") are possible.  </doc>
	    </parameter>
	    <parameter name="presentationType" type="string" default="points">
		    <doc> The plot types similar to "pch" parameter in R plottings. "points" or "p", "lines" or "l", "pointsLines" 
		    or "b", "histogram" or "h", "stairs" or "s", "otherStairs" or "S" (as in R), "overplotted" or "o", and 
		    "linesApart" or "c" (as in R) are the options. </doc>
	    </parameter>
	    <parameter name="plotClusterCentres" type="boolean" default="false">
		    <doc>If true, the centres of the clusters are plotted. 
                 The center is defined in every dimension using the 
                 mean value for the cluster.</doc>
	    </parameter>
        <parameter name="sectionTitle" type="string" default="">
            <doc>Title for a new latex section. If non is given, a new section
            is not created.</doc>
        </parameter>
        <parameter name="sectionType" type="string" default="section">
            <doc>Type of LaTeX section: usually one of section, subsection or
            subsubsection. No section statement is written if sectionTitle is 
            empty.</doc>
        </parameter>
        <parameter name="caption" type="string" default="Cluster plot for input file %s">
            <doc> LaTeX caption of each figure. The caption may contain one %s
            placeholder, which is replaced with the input file name.</doc>
        </parameter>
    </parameters>
</component>
