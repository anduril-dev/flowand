library(componentSkeleton)
library(sqldf)

# Filter outliers
outlier.remove <- function(tt,columnName,pvalue){
        a <- hist(tt[,columnName], plot=FALSE)
	possibility <- a$counts / sum(a$counts)

        flag <- possibility >= pvalue
        x <- (1:(length(a$breaks)-1))[flag]
        xmin <- a$breaks[min(x)]
        xmax <- a$breaks[max(x)+1]
        flag <- tt[,columnName]>=xmin & tt[,columnName]<xmax
        tt <- tt[flag,] 
        return(tt)
}

getCenter <- function(tt,channel){
        a <- hist(tt[,channel], plot=FALSE)
        flag <- a$density==max(a$density)
        x <- (1:(length(a$breaks)-1))[flag]
        xmin <- a$breaks[x]
        xmax <- a$breaks[x+1]

        flag <- tt[,channel]>=xmin & tt[,channel]<xmax
        center.sig <- median(tt[flag,channel])
        return(center.sig)
}

getClusterFromRatio <- function(cluster.center,variable,measure,rule){

	stopifnot(length(variable)==2)
	if(nrow(cluster.center)==1){
		return(rownames(cluster.center))
	}
	ratio <- cluster.center[,variable[1]]/cluster.center[,variable[2]]
	if(rule=="max"){
		out.cluster <- names(which(ratio==max(ratio)))
	}else if(rule=="min"){
		out.cluster <- names(which(ratio==max(ratio)))
	}
	
	return(out.cluster)
}

getClusterFromRange <- function(cluster.center, variable, start, end){
	stopifnot(length(variable)==1)
	flag <- cluster.center[,variable] > start & cluster.center[,variable] < end
	out.cluster <- rownames(cluster.center)[flag]
	return(out.cluster)
}

merge.cluster <- function(cluster.center,target.cluster,cutoff,channel,max.dist){

	flag <- rownames(cluster.center)!=target.cluster

	out.cluster <- target.cluster
	for(ind in (rownames(cluster.center)[flag])){
		tmp <- dist(cluster.center[c(target.cluster,ind),channel])[1]
		if((tmp/max.dist)<cutoff){
                        out.cluster <- c(out.cluster,ind)
                }
	}	

	return(out.cluster)
}


GatingRule <- function(cluster.center, cutoff, rule){
	tmp <- unique(rule[,c("ID","Rank")])
	rank <- tmp[,"Rank"]
	names(rank) <- tmp[,"ID"]
	rank <- sort(rank)
	popID <- names(rank)
	
	ruleList <- split(rule,rule[,"ID"])

	channel <- unique(unlist(strsplit(rule[,"Variable"],",")))
	max.dist <- max(dist(cluster.center[,channel]))

	# Gating population according to the rank
	pop.cluster <- list()
	pop.center <- list()
	for(i in 1:length(popID)){
		
		if(nrow(cluster.center)==0){
			pop.cluster[[i]] <- NA
			pop.center[[i]] <- NA
			names(pop.cluster)[i] <- popID[i]
			names(pop.center)[i] <- popID[i]
			break
		}

		id <- popID[i]
		r <- ruleList[[id]]
		
		meta <- rownames(cluster.center)
		ff <- 1
		for(j in 1:nrow(r)){
			if(!is.na(r[j,"Quantile"])){
				qt.ncell <- quantile(cluster.center[,"NCell"],prob=r[j,"Quantile"])
				num <- cluster.center[,"NCell"]
				names(num) <- rownames(cluster.center)
				flag <- which(num>=qt.ncell)
				tmp <- cluster.center[flag,]
				if(!is.matrix(tmp)){
					tmp <- t(as.matrix(tmp))
					rownames(tmp) <- names(flag)
				}
			}else{
				tmp <- cluster.center
			}

			mvar <- unlist(strsplit(r[j,"Variable"],","))
			mm <- r[j,"Measure"]
			mr <- r[j,"Rule"]

			if(mm=="ratio"){
				if(ff==1){
					meta <- intersect(meta,getClusterFromRatio(tmp,mvar,mm,mr))
				}else{
					sec <- tmp[intersect(meta,rownames(tmp)),]
					if(is.matrix(sec)){
						meta <- intersect(meta,getClusterFromRatio(sec,mvar,mm,mr))
					}else{
						meta <- intersect(meta,rownames(tmp))
					}
					
				}
				
			}else if(mm=="range"){
				d <- unlist(strsplit(r[j,"Rule"],","))
				if(is.na(as.numeric(d[1]))){
					start <- pop.center[[d[1]]][mvar]
					end <- pop.center[[d[2]]][mvar]
					meta <- intersect(meta,getClusterFromRange(tmp,mvar,start,end))
				}else{
					start <- as.numeric(d[1])
					end <- as.numeric(d[2])					
					meta <- intersect(meta,getClusterFromRange(tmp,mvar,start,end))
				}
				ff <- 0
			}
			if(length(meta)==0){
				break
			}	
		}

		# Merge close clusters
		if(length(meta)==0){
			pop.cluster[[i]] <- NA
			pop.center[[i]] <- NA
			names(pop.cluster)[i] <- popID[i]
			names(pop.center)[i] <- popID[i]

		}else{
			index <- merge.cluster(cluster.center,meta,cutoff,channel,max.dist)
			pop.cluster[[i]] <- index
			if(length(index)>1){
				pop.center[[i]] <- apply(cluster.center[index,colnames(cluster.center)[-ncol(cluster.center)]],2,mean)
			}else{
				pop.center[[i]] <- cluster.center[index,colnames(cluster.center)[-ncol(cluster.center)]]
			}
			names(pop.cluster)[i] <- popID[i]
			names(pop.center)[i] <- popID[i]
			ind <- rownames(cluster.center) %in% index
			index.left <- rownames(cluster.center)[!ind]
			if(length(index.left)==1){
				cluster.center <- t(as.matrix(cluster.center[index.left,]))
				rownames(cluster.center) <- index.left
			}else{
				cluster.center <- cluster.center[index.left,]
			}
		}			
	}

	# Output
	cluster.index <- c()
	for(i in 1:length(pop.cluster)){
		cluster.index <- c(cluster.index,paste(pop.cluster[[i]],collapse=","))
	}
	pop.name <- names(pop.cluster)

	out <- data.frame(PopIndex=pop.name,ClusterID=cluster.index)
	tmp <- unique(rule[,c("ID","Population")])
	stm <- "select PopIndex,Population,ClusterID from out join tmp on out.PopIndex=tmp.ID"
	out <- sqldf(stm)
        
        return(out)
}

execute <- function(cf) {
	
	input.folder <- get.input(cf,'clusters')
	data.files <- list.files(input.folder)
	
	if (input.defined(cf, 'array')) {
        input.facs.array <- Array.read(cf, 'array')
    }

	rule <- read.table(get.input(cf,'rule'),header=TRUE,sep="\t",stringsAsFactors=FALSE)
	#ruleList <- split(rule,rule[,"ID"])
	channel <- unique(unlist(strsplit(rule$Variable,",")))
	
	ClustCol <- get.parameter(cf,'clusterCol')
	cutoff <- get.parameter(cf,'cutOff')
	pvalue <- get.parameter(cf,'p')

	output.filtered.folder <- get.output(cf,'filtered')
	output.population.folder <- get.output(cf,'population')
	dir.create(output.filtered.folder, recursive=TRUE)
	dir.create(output.population.folder, recursive=TRUE)
	
	output.filtered.array <- get.output(cf,'filteredArray')
    dir.create(output.filtered.array)
    output.filtered.array.object <- Array.new()
    
    output.population.array <- get.output(cf,'populationArray')
    dir.create(output.population.array)
    output.population.array.object <- Array.new()

	if(input.defined(cf, 'clusters')) {
		data.files <- list.files(input.folder)
		data.fullpath <- paste(input.folder, data.files,sep="/")
	} else {
		data.files <- c()
		data.fullpath <- c()
	}
	
	if(input.defined(cf, 'array')) {
		data.files<-c(data.files,input.facs.array[,'Key'])
		data.fullpath<-c(data.fullpath,input.facs.array[,'File'])
	}
	
	for(i in 1:length(data.files)){
		print(i)
		
		#file <- paste(input.folder,"/",data.files[i],sep="")
		file <- data.fullpath[i]

        if (length(grep("\\.csv", data.files[i])) > 0) {
            csvstr <- ""
        } else {
            csvstr <- ".csv"
        }
        
		output.filtered.filename <- file.path(output.filtered.folder, paste(data.files[i], csvstr, sep=""))
		output.population.filename <- file.path(output.population.folder, paste(data.files[i], csvstr, sep=""))
		
		data <- CSV.read(file)
		flag <- channel %in% colnames(data)
		if(length(flag[!flag])!=0){
			print(paste("Channel ",paste(channel[!flag],collapse=",")," not found in file ",data.files[i],sep=""))
			next
		}

		clusters <- unique(data[,ClustCol])
		tmp <- split(1:nrow(data),data[,ClustCol])

		# Remove outliers
		channel <- rule[,"Variable"]
		channel <- unique(unlist(strsplit(channel,",")))

		cluster.sig.list <- list()
		cluster.center <- list()
		for(j in 1:length(clusters)){
        		clusterID <- as.character(clusters[j])
                if (!is.na(clusterID)) {
        		tt <- data[tmp[[clusterID]],]

			for(k in 1:length(channel)){
				tt <- outlier.remove(tt,channel[k],pvalue)
			}

			meta.center <- c()
			for(k in 1:length(channel)){
				meta.center[k] <- getCenter(tt,channel[k])
			}

        		cluster.sig.list[[j]] <- tt
        		cluster.center[[j]] <- c(meta.center, nrow(tt))
        }
		}

		cluster.center <- do.call(rbind,cluster.center)
		rownames(cluster.center) <- clusters
		colnames(cluster.center) <- c(channel,"NCell")

		# Gating
		gating <- GatingRule(cluster.center,cutoff,rule)
		out.data <- do.call(rbind,cluster.sig.list)

		out.cluster <- as.character(gating[,"ClusterID"])
		out.cluster <- as.integer(unlist(sapply(out.cluster,strsplit,",")))
		flag <- out.data[,ClustCol] %in% out.cluster
		out.data <- out.data[flag,]

		write.table(out.data, output.filtered.filename, sep="\t",row.names=F)
		output.filtered.array.object <- Array.add(output.filtered.array.object, 
										data.files[i],
										output.filtered.filename)
										
		write.table(gating, output.population.filename, sep="\t",row.names=F)
		output.population.array.object <- Array.add(output.population.array.object, 
										data.files[i],
										output.population.filename)
	}

	Array.write(cf, output.filtered.array.object, 'filteredArray')
	Array.write(cf, output.population.array.object, 'populationArray')

	return(0)        
}

main(execute)

