#!/bin/bash

URL="http://csbi.ltdk.helsinki.fi/pub/anduril_static/component_data/facs/AutoGating.tgz"
cd "$( dirname $0 )"
[[ -d testcases/case2_array ]] && {
    echo "Testcases already exist"
} || {
    mkdir -p testcases
    cd testcases
    wget -O - "$URL" | tar xzf - 
}

