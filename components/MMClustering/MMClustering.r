mmclustplot<-function(Data,dat,obj,Dist,plotType,pch.plot,file.name, cluster.numbers){
	if(plotType=='contour2d'){

		png(file.name, width=(ncol(dat))*500, 
		height=(ncol(dat))*500)
		par(mfrow=rep(ncol(dat),2)) 
		for(x.plot.counter in 1:(ncol(dat))){
			for(y.plot.counter in 1:(ncol(dat))){
				if(x.plot.counter==y.plot.counter){
					plot(1, type="n", axes=F, xlim=c(0,1), ylim=c(0,1), 						xlab="", ylab="")
					text(0.5,0.5,colnames(dat)[x.plot.counter], cex=2)
				}
				else{
					emmix.contour.2d(dat[,c(y.plot.counter,x.plot.counter)],
					obj$pro, obj$mu[c(y.plot.counter,x.plot.counter),], 
					obj$sigma[c(y.plot.counter,x.plot.counter),
					c(y.plot.counter,x.plot.counter),], 
					obj$dof, obj$delta[c(y.plot.counter,x.plot.counter),], obj$clust, ndist=Dist, 
					pch.plot=pch.plot, nrand=1000, xlab=colnames(dat)[y.plot.counter], 
					ylab=colnames(dat)[x.plot.counter], cluster.numbers=cluster.numbers)
				}
			}
		}
		dev.off()
	} else if(plotType=='regular2d'){
		png(file.name, width=(ncol(Data)+1)*500, 
		height=(ncol(Data)+1)*500)
		
		#Setting the appropriate colours for plotting
		if (length(unique(obj$clust))>32)
            colours=rainbow(length(unique(obj$clust)))
        else{
            library(RColorBrewer)
            colours=c(brewer.pal(8,"Dark2"),brewer.pal(12,"Set3"),brewer.pal(12,"Paired"))
        }

		par(mfrow=rep(ncol(Data),2)) 
		for(x.plot.counter in 1:(ncol(Data))){
			for(y.plot.counter in 1:(ncol(Data))){
				if(x.plot.counter==y.plot.counter){
					plot(1, type="n", axes=F, xlim=c(0,1), ylim=c(0,1), 						xlab="", ylab="")
					text(0.5,0.5,colnames(Data)[x.plot.counter], cex=2)
				}
				else{
					plot(Data[,y.plot.counter],Data[,x.plot.counter],
					col=colours[obj$clust], xlim=c(min(Data[,y.plot.counter]),
					max(Data[,y.plot.counter])+((max(Data[,y.plot.counter])-
					min(Data[,y.plot.counter]))/4)), ylim=
					c(min(Data[,x.plot.counter]),
					max(Data[,x.plot.counter])+((max(Data[,x.plot.counter])-
					min(Data[,x.plot.counter]))/4)), pch=pch.plot, xlab=colnames(Data)[y.plot.counter], 
					ylab=colnames(Data)[x.plot.counter])
					legend('topright',legend=paste('Cluster', 
					sort(unique(obj$clust), decreasing=F), '[',cluster.numbers[as.character(sort(unique(obj$clust), 
					decreasing=F))],',',format(cluster.numbers[as.character(sort(unique(obj$clust), 
					decreasing=F))]/sum(cluster.numbers), digits=1, scientific=F),']'), fill=colours[sort(unique(obj$clust), decreasing=F)], title="Cluster [frequency, ratio]")
				}
			}
		}
		dev.off()

	} else if(plotType=='regular3d'){
		
		if(ncol(Data)<3){
			write.error(cf,
			paste('Error in plotting! The channels to cluster', 
			'should be at least 3, for regular 3d plotting' ))
			
			return(PARAMETER_ERROR)}

		require(combinat)
		require(scatterplot3d)		
		plotting.dim=as.matrix(combn(ncol(Data),3))
		image.dim=trunc(sqrt(ncol(plotting.dim)))+ceiling(sqrt(ncol(plotting.dim))%%1)
		png(file.name, width=image.dim*500, 
		height=image.dim*500)
		par(mfrow=rep(image.dim,2))
		if (length(unique(obj$clust))>32)
            colours=rainbow(length(unique(obj$clust)))
        else{
            library(RColorBrewer)
            colours=c(brewer.pal(8,"Dark2"),brewer.pal(12,"Set3"),brewer.pal(12,"Paired"))
        }
		for(col.counter in 1:ncol(plotting.dim)){
			scatterplot3d(Data[,plotting.dim[,col.counter]], 
			color=colours[obj$clust], pch=pch.plot,
			main=paste(colnames(Data)[plotting.dim[,col.counter]], sep=','))
			legend('topright',legend=paste('Cluster', 
					sort(unique(obj$clust), decreasing=F), '[',cluster.numbers[as.character(sort(unique(obj$clust), 
					decreasing=F))],',',format(cluster.numbers[as.character(sort(unique(obj$clust), 
					decreasing=F))]/sum(cluster.numbers), digits=1, scientific=F),']'), fill=colours[sort(unique(obj$clust), decreasing=F)], title="Cluster [frequency, ratio]")
		}
		if (image.dim^2 > ncol(plotting.dim)) 
			for(text.counter in (ncol(plotting.dim)+1):(image.dim^2)){
				plot(1, type="n", axes=F, xlim=c(0,1), ylim=c(0,1), 					xlab="", ylab="")
					text(0.5,0.5,'AFACS', col='orange', cex=2)
				}
		
		dev.off()		
	}else if(plotType=='boundry2d'){

		png(file.name, width=(ncol(dat))*500, 
		height=(ncol(dat))*500)
		par(mfrow=rep(ncol(dat),2)) 
		for(x.plot.counter in 1:(ncol(dat))){
			for(y.plot.counter in 1:(ncol(dat))){
				if(x.plot.counter==y.plot.counter){
					plot(1, type="n", axes=F, xlim=c(0,1), ylim=c(0,1), 						xlab="", ylab="")
					text(0.5,0.5,colnames(dat)[x.plot.counter], cex=2)
				}
				else{
					plot.boundry.2d(dat[,c(y.plot.counter,x.plot.counter)],
					obj$pro, obj$mu[c(y.plot.counter,x.plot.counter),], 
					obj$sigma[c(y.plot.counter,x.plot.counter),
					c(y.plot.counter,x.plot.counter),], 
					obj$dof, obj$delta[c(y.plot.counter,x.plot.counter),], obj$clust, ndist=Dist, 
					pch.plot=pch.plot, nrand=1000, xlab=colnames(dat)[y.plot.counter], 
					ylab=colnames(dat)[x.plot.counter], cluster.numbers=cluster.numbers)
				}
			}
		}
		dev.off()
	} else if(plotType=='individualcontour2d'){

		png(file.name, width=(ncol(dat))*500, 
		height=(ncol(dat))*500)
		par(mfrow=rep(ncol(dat),2)) 
		for(x.plot.counter in 1:(ncol(dat))){
			for(y.plot.counter in 1:(ncol(dat))){
				if(x.plot.counter==y.plot.counter){
					plot(1, type="n", axes=F, xlim=c(0,1), ylim=c(0,1), 						xlab="", ylab="")
					text(0.5,0.5,colnames(dat)[x.plot.counter], cex=2)
				}
				else{
					individual.contour.2d(dat[,
					c(y.plot.counter,x.plot.counter)],
					obj$pro, obj$mu[c(y.plot.counter,x.plot.counter),], 
					obj$sigma[c(y.plot.counter,x.plot.counter),
					c(y.plot.counter,x.plot.counter),], 
					obj$dof, obj$delta[c(y.plot.counter,x.plot.counter),], obj$clust, ndist=Dist, 
					pch.plot=pch.plot, nrand=1000, xlab=colnames(dat)[y.plot.counter], 
					ylab=colnames(dat)[x.plot.counter], cluster.numbers=cluster.numbers)
				}
			}
		}
		dev.off()
	} else{	write.error(cf, paste('Error in Writing',file.name,
		': The plotting type is not valid'))
		return(PARAMETER_ERROR)}
	tex <- latex.figure(file.name,
	caption= paste('Mixture Model Clustering visualziation for output data file "',
	file.name,'"', sep=""),
	fig.label = sprintf('fig:%s', get.metadata(cf, 'instanceName')),
	image.width=20, image.height=20)
    	return(tex)
}

library(componentSkeleton)
execute <- function(cf){

# Read input
preprocessedData <- get.input(cf,'fcsFiles')

# Read parameters
g.min <- as.numeric(get.parameter(cf,'gMin'))
g.max <- as.numeric(get.parameter(cf,'gMax'))
Density <- get.parameter(cf,'density')	
channels.to.cluster <- get.parameter(cf,'channelsToCluster')
seed= as.numeric(get.parameter(cf,'randomSeed'))
mode.estimation <- get.parameter(cf,'estimateMode')	
step <- as.numeric(get.parameter(cf,'step'))
itmax <- as.numeric(get.parameter(cf,'iterationMaximum'))
epsilon <- as.numeric(get.parameter(cf,'epsilon'))
cluster.number= as.numeric(get.parameter(cf,'nSlaves'))
write.image <- get.parameter(cf,'writeImage')
write.params <- get.parameter(cf,'writeParams')
plot.type <- get.parameter(cf, 'plotType')
pagebreak=get.parameter(cf, 'pagebreak')
na.remove=get.parameter(cf, 'NaRmove', 'boolean')
include.pb=get.parameter(cf, 'includePosterior', 'boolean')
pch.plot=get.parameter(cf, 'pch', 'string')
idcol=get.parameter(cf, 'IDCol', 'string')
cluster.id.col.name <- get.parameter(cf, 'clusterIDColName', 'string')

if(suppressWarnings(!is.na(as.numeric(pch.plot))) ) pch.plot=as.numeric(pch.plot) else pch.plot=as.character(pch.plot)

if(plot.type=="regular2d" | plot.type=="regular3d") 
    channels.to.reg.plot <- get.parameter(cf,'channelsToRegularPlot')

# Read output

output.mm.files <- get.output(cf,'clusters')
output.visual <- get.output(cf,'report')
output.mm.params <- get.output(cf,'mmParams')
output.pb <- get.output(cf,'postProb')
output.mmspecs= get.output(cf, 'mmSpecs')
dir.create(output.mm.files)
dir.create(output.visual)
dir.create(output.mm.params)
dir.create(output.pb)
# Setting the values, and the required variables
current.dir=getwd()

# Importing the parallel function
source(paste(current.dir,"foldslave.R", sep='/'))

#Importing the Mixturemodel function
source(paste(current.dir,"EmSkew.R", sep='/'))

emskewlinks=dir('./', pattern="^emskew", full.names = TRUE)
preprocessedFiles <- dir(preprocessedData, pattern=".csv", full.names = TRUE)

if (length(preprocessedFiles)==0){
    write.error(cf, 'Error: The input fcsFiles does not contain ".csv" files') 
    return(PARAMETER_ERROR)
}

g.range <- g.min:g.max
channels.to.cluster <- gsub("^[ \t]+|[ \t]+$", "", channels.to.cluster)
channels.to.cluster <- gsub(" *, *", ",", channels.to.cluster)
channels.to.cluster<-unlist(strsplit(channels.to.cluster, split=','))
num.channels.to.cluster = length(channels.to.cluster)

if(plot.type=="regular2d" | plot.type=="regular3d") {
    channels.to.reg.plot <- gsub("^[ \t]+|[ \t]+$", "", channels.to.reg.plot)
    channels.to.reg.plot <- gsub(" *, *", ",", channels.to.reg.plot)
    channels.to.reg.plot<-unlist(strsplit(channels.to.reg.plot, split=','))
    }

write.table(matrix(c('output.file','input.file','expected.cluster','real.cluster','ICL','aic','bic','SWR'), ncol=8, nrow=1), 
file= output.mmspecs, col.names=F, row.names=F, 
quote=F, sep = "\t", append=T)


if (mode.estimation=='true') mode.estimation=T else mode.estimation=F

if (write.image=='true') write.image=T else write.image=F

if (write.params=='true') write.params=T else write.params=F

if (pagebreak=='true') pagebreak=T else pagebreak=F

	
Dist = switch(Density, "normal" = 1, "t" = 2, "skewn" = 3,"skewt" = 4)

Tex <- "\\subsection{Mixture Model Clustering visualization}"
if (pagebreak) {
	Tex <- c('\\clearpage{}', Tex)
}



#Parallelising

mmdata=matrix(0, ncol=2, nrow=length(preprocessedFiles)*length(g.range))

for(counter1 in 1:length(preprocessedFiles)){
	for(counter2 in 1:length(g.range)){
		mmdata[((counter1-1)*length(g.range))+counter2,1]=preprocessedFiles[counter1]
		mmdata[((counter1-1)*length(g.range))+counter2,2]=g.range[counter2]
}
}

library("Rmpi")
extentionvalue=(cluster.number-(nrow(mmdata)%%
cluster.number))%%cluster.number
if(extentionvalue>0){
for(tmpcount in 1:extentionvalue){
mmdata=rbind(mmdata,c(NA,NA))
}
}


mpi.spawn.Rslaves(nslaves=cluster.number)
mpi.bcast.Robj2slave(cluster.number)
mpi.bcast.Robj2slave(mmdata)
mpi.bcast.Robj2slave(g.range)
mpi.bcast.Robj2slave(Dist)
mpi.bcast.Robj2slave(seed)
mpi.bcast.Robj2slave(mode.estimation)
mpi.bcast.Robj2slave(step)
mpi.bcast.Robj2slave(itmax)
mpi.bcast.Robj2slave(cf)
mpi.bcast.Robj2slave(epsilon)
mpi.bcast.Robj2slave(emskewlinks)
mpi.bcast.Robj2slave(current.dir)
mpi.bcast.Robj2slave(mmclustplot)
mpi.bcast.Robj2slave(na.remove)
mpi.bcast.Robj2slave(foldslave)
mpi.bcast.Robj2slave(output.mm.files)
mpi.bcast.Robj2slave(output.mmspecs)
mpi.bcast.Robj2slave(cluster.id.col.name)
mpi.bcast.Robj2slave(pch.plot)
mpi.bcast.Robj2slave(idcol)
mpi.bcast.Robj2slave(channels.to.cluster)
mpi.bcast.Robj2slave(output.pb)
mpi.bcast.Robj2slave(include.pb)
if(plot.type=="regular2d" | plot.type=="regular3d") 
    mpi.bcast.Robj2slave(channels.to.reg.plot)
mpi.bcast.Robj2slave(write.image)
mpi.bcast.Robj2slave(output.visual)
mpi.bcast.Robj2slave(output.mm.params)
mpi.bcast.Robj2slave(write.params)
mpi.bcast.Robj2slave(plot.type)

mpi.bcast.cmd(id<-mpi.comm.rank())
rssresult <- mpi.remote.exec(foldslave())
mpi.close.Rslaves()
Tex=c(Tex, paste(gsub('/.*/','',unlist(rssresult)), collapse='\n'))


latex.write.main(cf, 'report', Tex)

temp.mmspecs= CSV.read(output.mmspecs)
temp.mmspecs=temp.mmspecs[order(temp.mmspecs[,3],  decreasing = FALSE),]
temp.mmspecs=temp.mmspecs[order(temp.mmspecs[,1],  decreasing = FALSE),]
CSV.write(output.mmspecs,temp.mmspecs)
}

main(execute)
