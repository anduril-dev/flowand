<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>MMClustering</name>
    <version>1.0</version>
    <doc>
    Normal, t, Skew-normal, and skew-t Mixture-Model Clustering.
    </doc>
    <author email="ali.oghabian@helsinki.fi">Ali Oghabian</author>
    <category>Clustering</category>
    <category>FlowCytometry</category>
    <launcher type="R">
        <argument name="file" value="MMClustering.r" />
    </launcher>
    <requires>R, Rmpi</requires>
    <inputs>
        <input name="fcsFiles" type="CSVList">
            <doc>Location of the csv files to be clustered e.g. Preprocessed files in 
		flow cytometry analysis .</doc>
        </input>
    </inputs>
    <outputs>
        <output name="clusters" type="CSVList">
            <doc>csv files including the input data and one additional column 
		presenting the mixture modeling cluster results. The numbers added to the 
		 input filenames represent the g parameter number.</doc>
        </output>
        <output name="postProb" type="CSVList">
            <doc>Posterior probabilities of the data. For each file in fcsFiles generates a file with similar name ehich includes the psterior probability of each point of the data. The dimentions of each csv file is similar to dimentions of the clsuetred data i.e. (size of the ChannelsToCluster)*( Row number of the data). </doc>
        </output>
	<output name="report" type="Latex">
	    <doc>Visualization of MixtureModeling results.</doc>
	</output>
        <output name="mmSpecs" type="CSV">
            <doc>The mmSpecs.csv file, include the information and calculations for the 
		Mixture Modeling Clustering that are suitable for OptimalClustering component.
		The first column includes the file names and the other columns are the aic, bic 
		and SWR calculations, which could be used to estimate the best clustering g 
		parameter</doc>
        </output>
        <output name="mmParams" type="BinaryFolder">
            <doc>.txt files including parameters for the Mixture model results. They are in R 
		object format, that have been presented in an R scripts text.</doc>
        </output>

    </outputs>
    <parameters>
	<parameter name="gMin" type="int" default="3">
		<doc>The minimum number of the Mixture Model clusters.It is considering that 
		results for some clusters might be Null. As for instance, the results for 
		MMclustering with g parameter (i.e. expected cluster number) of 5 might group the 
		rows of the data in 4 clusters of 1,2,4,and 5.</doc>
	</parameter>
	<parameter name="gMax" type="int" default="5">
		<doc>The maximum number of the "Mixture Model" clusters. It is considering that 
		results for some clusters might be Null. As for instance, the results for 
		MMclustering with g parameter (i.e. expected cluster number) of 5 might group the 
		rows of the data in 4 clusters of 1,2,4,and 5.</doc>
	</parameter>
	<parameter name="density" type="string" default="skewt">
		<doc>Density distribution to be used for clustering: normal, t, skew
			 normal, skew t.</doc>
	</parameter>
	<parameter name="channelsToCluster" type="string">
		<doc>A comma-separated list of column names or column numbers (e.g. channel names/
		numbers in flow cytometry analysis), to be clustered. As for instance the value 
		could be set as "1, 2, 3, 4, 5" or "FSC.A, SSC, ERK1, STAT1, CD4" .</doc>
	</parameter>
	<parameter name="IDCol" type="string" default="">
	    <doc> The column representing the row numbers. The parameter is optional and it would be added to the 
	    "postProb" files, if provided. </doc>
	</parameter>
	<parameter name="channelsToRegularPlot" type="string" default="">
		<doc>A comma-separated list of column names or column numbers (e.g. channel names/
		numbers in flow cytometry analysis), to be plotted. As for instance the value 
		could be set as "1, 2, 3, 4, 5" or "FSC.A, SSC, ERK1, STAT1, CD4". This parameter is only for the "regular2d" and "regullar3d" plottings.</doc>
	</parameter>	
	<parameter name="NaRmove" type="boolean" default="true">
		<doc>Whether remove the rows with na from the data or return an Error.</doc>
	</parameter>
	<parameter name="randomSeed" type="int" default="1984">
		<doc>The seed value for semi-random processes</doc>
	</parameter>
	<parameter name="estimateMode" type="boolean" default="false">
		<doc>Used only for skew distributions. Whether to estimate the mode
	 		 for each cluster. The defalt value (i.e. false) is recommended</doc>
	</parameter>
	<parameter name="step" type="float" default="0.5">
		<doc>Only used when Density is "skew" and the estimateMode is "true".
			 It is used to calculated the number of the iterations of the 
			 Expected-Maximization (i.e. EM), while computing the Maximum 
			 Likelihood (i.e ML) estimate. The value should be greater than 0. 
			 The smaller the value, the more accurate the estimations would be.</doc>
	</parameter>
	<parameter name="pagebreak" type="boolean" default="false">
            <doc>Tells if the result document should start with a page break.</doc>
        </parameter>
	<parameter name="iterationMaximum" type="int" default="5000">
		<doc>The maximum limitation for the number of iterations, in the mixturemodeling fitting process </doc>
	</parameter>
	<parameter name="epsilon" type="float" default="0.0001">
		<doc>A value used for threshold in the mixture modeling fitting process. The 
		smaller the value the more accurate the cluster results are. If the clusters do 
		not satisfy the epsilon threshold in iterationMaximum iteration attempts an 
		error would be returned.</doc>
	</parameter>		
	<parameter name="nSlaves" type="int" default="5">
		<doc> The number of the clusters used for paralelising the Mixture 
			  Modeling.</doc>
	</parameter>
	<parameter name="writeParams" type="boolean" default="false">
		<doc>Whether write the Mixture Model parameters.</doc>
	</parameter>
	<parameter name="writeImage" type="boolean" default="false">
		<doc>Whether include the plotting images in the report directory.</doc>
	</parameter>
    <parameter name="pch" type="string" default=".">
		<doc>The type and shape of plotting points. Numbers (e.g. "2") and characters (e.g. ".") are possible.  </doc>
	</parameter>
	<parameter name="plotType" type="string" default="boundry2d">
		<doc>The type of Plotting. The choices are regular2d, regular3d, 
			contour2d, individualcontour2d, and boundry2d.</doc>
	</parameter>
    <parameter name="includePosterior" type="boolean" default="true">
		<doc>Whether include the Posterior probabilities. Check "the postProb" output for more information. </doc>
	</parameter>
    <parameter name="clusterIDColName" type="string" default="cluster">
            <doc>The name of the column in the <code>clusters</code> output
                 which contains the cluster IDs of the rows.</doc>
    </parameter>
    </parameters>
</component>
