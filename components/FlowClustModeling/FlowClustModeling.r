library(componentSkeleton)

flowClust.res.plot <- function(cf,resfc,data,channels.to.cluster,plot.obj,index){
	n.column <- length(channels.to.cluster)
	
	jpeg(plot.obj$file,width=n.column*plot.obj$width,height=n.column*plot.obj$height,quality=100)
	par(mfrow=c(n.column,n.column),mar=c(1.5,1.5,1.5,1.5), oma=c(1,1,3,1))


    for(i in 1:n.column){
        for(j in 1:n.column){
			if(i==j){
				plot(x=c(1,2),y=c(1,2),type="n",xaxt="n",yaxt="n",xlab="",ylab="")
				label.widths <- strwidth(channels.to.cluster, "user")
               			cex.labels <- max(0.8, 1/max(label.widths))
				text(1.5,1.5,channels.to.cluster[i],cex=cex.labels)
			}else{

            plot(resfc, data=data, subset=c(channels.to.cluster[j],channels.to.cluster[i]))
            }
	    }
    }

	mtext(plot.obj$main.log,side=3,outer=TRUE,cex=1.5)
	dev.off()

	caption <- paste("FlowClust results visualization : ",plot.obj$sample.name, sep="")
    fig.label <- sprintf('fig:%s-%s', get.metadata(cf, 'instanceName'), plot.obj$sample.name)
    tex <- latex.figure(plot.obj$file.name, caption=caption, fig.label=fig.label, image.width=18, image.height=18)

	return(tex)

}
execute <- function(cf) {

	library(flowCore)
	library(flowClust)

	input.path <- get.input(cf,'fcsDir')
	output.file.path <- get.output(cf,'clusters')
	dir.create(output.file.path)
	output.plot.path <- get.output(cf, 'report')
	dir.create(output.plot.path)
	
    # parameters for clustering
	channels.to.cluster <- get.parameter(cf,'channelsToCluster')
	clust.K.min <- get.parameter(cf, 'minClusters')
	clust.K.max <- get.parameter(cf, 'maxClusters')
	clust.K <- clust.K.min:clust.K.max
	clust.B <- as.numeric(get.parameter(cf, 'numIterations'))
	clusterIDColName <- get.parameter(cf, 'clusterIDColName')
    cluster.level <- as.numeric(get.parameter(cf, 'level'))
	
    # parameters for image
	width <- as.numeric(get.parameter(cf,'width'))
	height <- as.numeric(get.parameter(cf,'height'))
	keepOriginalChannelNames <- get.parameter(cf, 'keepOriginalChannelNames', 'boolean') 
    instance.name <- get.metadata(cf, 'instanceName')

    write.log(cf, "Proceeding automated gating of data ...")

	channels.to.cluster <- gsub(" *, *", ",", channels.to.cluster)
	channels.to.cluster <- strsplit(channels.to.cluster,',')[[1]]
	num.channels.to.cluster = length(channels.to.cluster)
	
	data.files <- list.files(input.path,".csv$")

	tex <- "\\subsection{Visualization}"
    	if (get.parameter(cf, 'pagebreak', 'boolean')) {
      		tex <- c('\\clearpage{}', tex)
    	}

	#samp <- read.table(files=paste(input.path,data.files,sep="/"))
	
	for(i in 1:length(data.files)){
	    set.seed(38419)
        print(data.files[i])
		fData <- CSV.read(paste(input.path,data.files[i],sep="/"))
        clust.chan <- colnames(fData)[which(colnames(fData) %in% channels.to.cluster)]

		res1 <- flowClust(fData,varNames=clust.chan, K = clust.K, B=clust.B, trans=0, level=cluster.level)

        # plot clusters
		write.log(cf,"Plotting...")
        sample.name <- strsplit(data.files[i], "\\.csv")[[1]]
		file.name <- sprintf('%s-%s.jpg', instance.name, sample.name)
		obj <- list(file=sprintf('%s/%s', output.plot.path, file.name), main.log=sample.name,width=width,height=height,file.name=file.name, sample.name=sample.name)
		tex <- c(tex, flowClust.res.plot(cf,res1,fData,clust.chan,obj,i))

        # split into clusters
        pops <- split(fData, res1)
        out.clusters <- c()
        for(j in 1:length(pops)){
            pop.current <- pops[[j]]
            clust.vector <- rep(j,length(pop.current[,1]))
            out.clusters.tmp <- cbind(pop.current, clust.vector)
            out.clusters <- rbind(out.clusters,out.clusters.tmp)
        }
		colnames(out.clusters) <- c(colnames(pop.current), clusterIDColName)
		write.table(out.clusters, paste(output.file.path,"/",sample.name,".csv",sep=""),sep="\t",row.names=FALSE)
	}

		

	latex.write.main(cf, 'report', tex)
	return(0)
}

main(execute)
