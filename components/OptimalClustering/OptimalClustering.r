library(componentSkeleton)
library(fpc)

# Constants
SAMPLE.FILE.NAME.COLUMN = "input.file"
CLUST.RESULT.FILE.COLUMN = "output.file"
#CLUSTER.ID.COLUMN = "preClust"
CLUSTER.NUMBER.COLUMN = "real.cluster"
AIC.COLUMN = "aic"
BIC.COLUMN = "bic"
SWR.COLUMN = "SWR"
AID.COLUMN = "AID"
IIR.COLUMN = "IIR"
ICL.COLUMN = "ICL"

ALLOWED.METRICS <- c("BIC", "AIC", "SWR", "IIR", "AID", "ICL")
ALLOWED.METHODS <- c("min", "max", "changepoint")


execute <- function(cf) {
    # Read input
    clust.dir <- get.input(cf,'clusters')
    clust.stat <- CSV.read(get.input(cf, 'clustStat'))

    # Read parameters
    metric <- get.parameter(cf, 'metric', allowed.values=ALLOWED.METRICS)
    if(metric == "BIC") metric <- "bic"
    if(metric == "AIC") metric <- "aic"
    method <- get.parameter(cf, 'method', allowed.values=ALLOWED.METHODS)
    seed <- as.integer(get.parameter(cf,'seed'))
    n.sample <- as.integer(get.parameter(cf, 'nSample'))
    use.aid.and.iir <- get.parameter(cf, 'useAIDAndIIR', 'boolean')
    cluster.id.column <- get.parameter(cf, 'clusterClustCol', 'string')

    # Create output directories
    output.dir <- get.output(cf,'clusters')
    dir.create(output.dir)
    report.dir <- get.output(cf, 'report')
    dir.create(report.dir)

  	# Execute
    
    # Calculate AID (average intercluster distance) and IRR (average intra
    # cluster distance / average intercluster distance).
    if(use.aid.and.iir) {    
        for(i in 1:nrow(clust.stat)) {
            write.log(cf, sprintf("Calculating AID and IIR for %s...", clust.stat[i, CLUST.RESULT.FILE.COLUMN])) 
            clust.res.file <- paste(clust.dir, .Platform$file.sep, clust.stat[i, CLUST.RESULT.FILE.COLUMN], sep="")
            clust.metric <- get.aid.and.iir(clust.res.file, cluster.id.column, seed, n.sample)
            clust.stat[i, "AID"] <- clust.metric$AID
            clust.stat[i, "IIR"] <- clust.metric$IIR
        }
    }

    useColumns <- c(AIC.COLUMN, BIC.COLUMN, ICL.COLUMN, SWR.COLUMN)
    if(use.aid.and.iir) {
        useColumns <- c(useColumns, AID.COLUMN, IIR.COLUMN)
    }
    optimal.clust.files <- get.optimal.clustering.files(cf, clust.stat, metric, method)
    # Get the original input file names matching to the original clustering files
    original.file.names <- clust.stat[match(optimal.clust.files, clust.stat[,CLUST.RESULT.FILE.COLUMN]), SAMPLE.FILE.NAME.COLUMN]
    file.copy(from=paste(clust.dir, "/", optimal.clust.files, sep=""), to=output.dir)
    for(i in 1:length(optimal.clust.files)) {
        file.rename(from=paste(output.dir, "/", optimal.clust.files[i], sep=""), to=paste(output.dir, "/", original.file.names[i], sep=""))
    }
    
    # Create results table
    optimal.clust.res.table <- get.optimal.clustering.res.table(cf, clust.stat, metric, method)
    
    # Write latex report
    tex <- latex.table(optimal.clust.res.table, paste(rep("c", ncol(optimal.clust.res.table)), collapse=""))
    tex <- c(tex, create.report(cf, clust.stat, useColumns, CLUSTER.NUMBER.COLUMN))
    latex.write.main(cf, 'report', tex)
}

create.report <- function(cf, clust.stat, columns, cluster.column) {
    pdf.filename <- sprintf('OptimalClustering-%s.pdf', get.metadata(cf, 'instanceName'))
    pdf.file <- file.path(get.output(cf, 'report'), pdf.filename)
    pdf(file=pdf.file)
    
    samples <- unique(clust.stat[,SAMPLE.FILE.NAME.COLUMN])
    col <- rainbow(length(samples))
    par(mfrow=c(ceiling(length(columns)/2)+1, 2))
    par(mai=c(0.5,0.5,0.1,0.1)) # Set the margins of the subplots.
    par(mgp=c(2.5,1,0)) # Set the margins of axis.
    tex <- character()
    xlim <- range(clust.stat[,cluster.column])
    
    for(column in columns) {
        for(i in 1:length(samples)) {
            sub.clust.stat <- clust.stat[clust.stat[,SAMPLE.FILE.NAME.COLUMN] == samples[i],]
            sub.clust.stat <- sub.clust.stat[order(sub.clust.stat[,cluster.column]),]
        
            ylim <- c(min(clust.stat[,column]), max(clust.stat[,column])) / mean(clust.stat[,column])
            y <- sub.clust.stat[,column]
            type <- "p"
            lty <- 1

            # If no metric values are found.
            if(all(is.na(ylim))) {
                ylim <- c(0,1)
            }
            if(all(is.na(y))) {
                y[1:length(y)] <- 0
                type <- "n"
                lty <- 0
            }
            if(i == 1) {
                plot(sub.clust.stat[,cluster.column], y/mean(y), col=col[i], type=type, xlim=xlim, ylim=ylim, xlab="Number of clusters", ylab=column)
                lines(sub.clust.stat[,cluster.column], y/mean(y), col=col[i], lty=lty)
            } else {
                points(sub.clust.stat[,cluster.column], y/mean(y), col=col[i], type=type, xlim=xlim, ylim=ylim)
                lines(sub.clust.stat[,cluster.column], y/mean(y), col=col[i], lty=lty)
            }
        }
    }
    frame()
    legend('topleft', legend=samples, lty=1, pch=1, col=col)
    tex <- c(tex, latex.figure(filename=pdf.filename, caption="Optimal cluster number selection metrics plotted for the samples as a function of the number of the clusters used in the mixture modelling", image.width=20, image.height=20))
}

get.aid.and.iir <- function(datafile, cluster.id.column, seed, n.sample) {
    data <- CSV.read(datafile)
    # Take a random sample of the data
    if(nrow(data) > n.sample) {
        set.seed(seed)
        data <- data[sample(1:nrow(data), size=n.sample),]
    }
    data.columns <- setdiff(1:ncol(data), cluster.id.column)
    data.points <- data[,data.columns]
    data.clusters <- data[,cluster.id.column]
    cs <- cluster.stats(d = dist(data.points), clustering = data.clusters, silhouette=F)
    list(AID=cs$average.between, IIR=cs$wb.ratio)
}

get.optimal.clustering.files <- function(cf, clust.stat, metric, method) {
    samples <- unique(clust.stat[,SAMPLE.FILE.NAME.COLUMN])
    optimal.clust <- character()
    for(i in 1:length(samples)) {
        sub.clust.stat <- clust.stat[clust.stat[,SAMPLE.FILE.NAME.COLUMN] == samples[i],]
        best.index <- NA
        if(method=="min") {
            best.index <- order(sub.clust.stat[,metric])[1]
        }else if(method=="max") {
            best.index <- order(sub.clust.stat[,metric], decreasing=F)[1]
        }else if(method=="changepoint") {
            best.index <- match(get.changepoint(cf, sub.clust.stat, metric), sub.clust.stat[,CLUSTER.NUMBER.COLUMN])
        }
        optimal.clust <- c(optimal.clust, sub.clust.stat[best.index, CLUST.RESULT.FILE.COLUMN])
    }
    optimal.clust
}

get.optimal.clustering.res.table <- function(cf, clust.stat, metric, method) {
    samples <- unique(clust.stat[,SAMPLE.FILE.NAME.COLUMN])
    optimal.clust.res.table <- NULL
    for(i in 1:length(samples)) {
        sub.clust.stat <- clust.stat[clust.stat[,SAMPLE.FILE.NAME.COLUMN] == samples[i],]
        if(method=="min") {
            best.index <- order(sub.clust.stat[,metric])[1]
        }else if(method=="max") {
            best.index <- order(sub.clust.stat[,metric], decreasing=F)[1]
        }else if(method=="changepoint") {
            best.index <- match(get.changepoint(cf, sub.clust.stat, metric), sub.clust.stat[,CLUSTER.NUMBER.COLUMN])
        }
        row <- sub.clust.stat[best.index,]
        if(is.null(optimal.clust.res.table)) {
            optimal.clust.res.table <- row
        } else {
            optimal.clust.res.table <- rbind(optimal.clust.res.table, row)
        }
    }
    optimal.clust.res.table
}

get.changepoint <- function(cf, clust.stat, metric) {
    clust.stat <- clust.stat[order(clust.stat[,CLUSTER.NUMBER.COLUMN]),]
    min.ss <- NA
    changepoint <- NA

    # To fit a changepoint model we must have at least 4 models.
    if(length(unique(clust.stat[,CLUSTER.NUMBER.COLUMN])) < 3) {
        write.error(cf, sprintf("Cannot fit change point model. There are only %d models with different number of clusters. At least 4 models with different number of clusters are needed.", length(unique(clust.stat[,CLUSTER.NUMBER.COLUMN]))))
    }

    for(i in 2:(length(clust.stat)-1)) {
        data.first <- clust.stat[1:i,]
        data.second <- clust.stat[i:length(clust.stat[,CLUSTER.NUMBER.COLUMN]),]

        lm.res.first <- lm(data.first[,metric]~data.first[,CLUSTER.NUMBER.COLUMN], data.first)
        lm.res.second <- lm(data.second[,metric]~data.second[,CLUSTER.NUMBER.COLUMN], data.second)

        res.ss.first <-  sum(lm.res.first$residuals^2)
        res.ss.second <- sum(lm.res.second$residuals^2)
        res.ss <-  res.ss.first + res.ss.second
        if(is.na(min.ss) | res.ss < min.ss) {
            min.ss <- res.ss
            changepoint <- clust.stat[i, CLUSTER.NUMBER.COLUMN]
        }
    }
    return(changepoint)
}
main(execute)
