import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;

import java.lang.String;
import java.io.File;

import flowGraph.PopUpWindow;

public class ClusterChooser extends SkeletonComponent {

	public static final String PRE_POPULATION = "population"; 
	public static final String CLUSTER_PLOTS = "plots";
	public static final String CORRECTED_POP = "corrected";
	public static final String SAMPLE_ANNOTATION = "sampleAnnotation";
	public static final String FILE_STATUS = "fileStatus";

	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		String [] inputs = new String[5];
		inputs[1] = cf.getInput(PRE_POPULATION).toString();
		inputs[0] = cf.getInput(CLUSTER_PLOTS).toString();
		inputs[2] = cf.getInput(SAMPLE_ANNOTATION).toString();
		inputs[3] = cf.getOutput(CORRECTED_POP).toString();
		inputs[4] = cf.getOutput(FILE_STATUS).toString();

		boolean status = new File(inputs[3]).mkdirs();

		PopUpWindow popUp = new PopUpWindow(inputs, cf.getParameter("fileColumn"), cf.getParameter("annoColumn"));

		return ErrorCode.OK;
	}

	/**
        * @param argv Pipeline arguments
        */
        public static void main(String[] argv) {
        new ClusterChooser().run(argv);
        }
}
