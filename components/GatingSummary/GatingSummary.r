library(componentSkeleton)

generate.column.name <- function(list){
	
	list.names <- names(list)
	column.names <- c()

	for(i in 1:length(list)){
		tmp <- paste(list.names[i],list[[i]],sep="_")
		column.names <- c(column.names,tmp)		
	}
	
	return(column.names)
}

generate.summary.table <- function(Result, FileName, FileFolder, sampleInfo, PanelInfo, Cluster,combine.type){

	files <- paste(FileFolder,FileName,sep="")

	for(i in 1:length(files)){
		this.file <- FileName[i]
		t <- CSV.read(files[i])
		panel <- sampleInfo[i,"Panel"]
		colnames(t) <- paste(panel,colnames(t),sep="_")
		ab <- paste(panel,strsplit(PanelInfo[panel,"Antibody"],",")[[1]],sep="_")
		
		stm <- "SELECT Stimulus FROM sampleInfo WHERE File='"
		stm <- paste(stm,FileName[i],"'",sep="")
		stimulus <- sqldf(stm)[1,1]

		myCluster <- strsplit(as.character(Cluster[[this.file]]),",")[[1]]
		colnames(t)[ncol(t)] <- "cluster"
		stm <- "SELECT * FROM t WHERE cluster in ('"
		tmp <- paste(myCluster,collapse="','")
		stm <- paste(stm,tmp,"')",sep="")
		res <- sqldf(stm)
		res <- apply(res,2,combine.type,na.rm=TRUE)
		
		Result[stimulus,ab] <- res[ab]
	}
	return(Result)
}

tex.generator <- function(cf, tex, SummaryTable, pdf.path, file.name, cell.type,width, color.schema,rotate, na.color){
	
	x <- 1:ncol(SummaryTable)
	y <- 1:nrow(SummaryTable)

	if(color.schema=="greenred"){
		col <- greenred(1000)
	}else if(color.schema=="redgreen"){
		col <- redgreen(1000)
	}else if(color.schema=="redblue"){
		col <- redblue(1000)
	}else if(color.schema=="bluered"){
		col <- bluered(1000)
	}else if(color.schema=="heatcolor"){
		col <- heat.colors(1000)
	}else if(color.schema=="rainbow"){
		col <- rainbow(1000)
	}else if(color.schema=="blackyellowwhite"){
        col <- colorRampPalette(c("black","yellow","white"))(1000)
    }


	pdf(paste(pdf.path,"/",get.metadata(cf, 'instanceName'),"-",file.name,"-",cell.type,".pdf",sep=""),width=width*ncol(SummaryTable),height=width*nrow(SummaryTable))
	#image.plot(x,y,t(SummaryTable)[,nrow(SummaryTable):1],xaxt="n",yaxt="n",main=cell.type,xlab="",ylab="",col=col)
	par(mar=c(2,1,3,2))
	
    if (length(SummaryTable[which(!is.na(SummaryTable))]) > 0) {
		heatmap.2(SummaryTable, col=col, scale="none",key=TRUE, symkey=FALSE, density.info="none", trace="none", cexRow=0.8, Colv=FALSE, Rowv=FALSE, dendrogram="none",cexCol=0.8,na.rm=TRUE,na.color=na.color,main=cell.type)	
    } else { # if none of the files had data for specific cell population
		plot(0:10,0:10, type = "n", xlab = "", ylab = "",main=cell.type)
		text(5,5,"All values are NA", cex=5)
	}
	dev.off()
	caption <- paste(file.name,": ",cell.type,sep="")
	tex <- c(tex, latex.figure(paste(get.metadata(cf, 'instanceName'),"-",file.name,"-",cell.type,".pdf",sep=""), caption=caption, image.width=20, image.height=20))

	row.names <- rownames(SummaryTable)
	col.names <- colnames(SummaryTable)
	tmp <- as.numeric(as.matrix(SummaryTable))
	tmp <- round(tmp*100)/100
	tmp[is.na(tmp)] <- "-"
	tmp <- matrix(tmp,nrow=nrow(SummaryTable))
	colnames(tmp) <- col.names
	rownames(tmp) <- row.names
	
	if(rotate){
		SummaryTable <- t(tmp)
		col.names <- gsub("_","-",col.names)
		SummaryTable <- cbind(Stimulus=col.names,SummaryTable)
		
	}else{
		SummaryTable <- cbind(Stimulus=row.names,tmp)
	}
	tex <- c(tex, latex.tabular(SummaryTable, paste(rep("l",ncol(SummaryTable)),collapse=""), long=TRUE, escape.content=FALSE))

	return(tex)
}

execute <- function(cf) {

	library(sqldf)
	library(fields)
	library(gplots)
        
	summaries.dir <- paste(get.input(cf, 'summaries'),"/",sep="")
	sample.annotation <- CSV.read(get.input(cf, 'sampleAnnotation'))
	panel.info <- CSV.read(get.input(cf, 'panel'))
	rownames(panel.info) <- panel.info[,1]
	popDir <- paste(get.input(cf, 'population'),"/",sep="")
	population.files <- list.files(popDir)

	out.path <- get.output(cf,'report')
	dir.create(out.path, recursive=TRUE)
	table.out.dir <- paste(get.output(cf,'summaryResult'),"/",sep="")
	dir.create(table.out.dir, recursive=TRUE)
	
	identifier <- get.parameter(cf,'targetPop')
	width <- get.parameter(cf,'width','float')
	color.schema <- get.parameter(cf,'colorSchema')
	combine.type <- get.parameter(cf,'type')
	rotate <- get.parameter(cf,'rotate','boolean')
	na.color <- get.parameter(cf,'NAcolor','string')

	idList <- c()
	for(i in 1:length(population.files)){
		tmp <- CSV.read(paste(popDir,population.files[i],sep=""))
		index <- tmp[,"Population"]==identifier
		idList[i] <- tmp[index,"ClusterID"]
	}
	ClusterIndex <- data.frame(File=population.files,Cluster=idList,stringsAsFactors=FALSE)

    sectionLabel = paste("Summary",identifier, sep=", ")
	tex.initial <- paste("\\subsection{",sectionLabel,"}",sep="")
    	if (get.parameter(cf, 'pagebreak', 'boolean')) {
      		tex.initial <- c('\\clearpage{}', tex.initial)
    	}
	tex <- c()

	file.list <- split(1:nrow(sample.annotation), sample.annotation[,4])
	
	for(i in 1:length(file.list)){
		stimulus <- unique(sample.annotation[file.list[[i]],"Stimulus"])
		panel.name <- unique(sample.annotation[file.list[[i]],"Panel"])
		antibody <- panel.info[panel.name,"Antibody"]
		names(antibody) <- panel.name
		tmp <- strsplit(antibody,",")
		column.names <- generate.column.name(tmp)
		
		table <- matrix(rep(NA,length(stimulus)*length(column.names)),nrow=length(stimulus))
		rownames(table) <- stimulus
		colnames(table) <- column.names

		stm <- "SELECT * FROM ClusterIndex WHERE File in ('"
		tmp <- paste(sample.annotation[file.list[[i]],"File"],collapse="','")
		stm <- paste(stm,tmp,"')",sep="")
		tmp <- sqldf(stm)
		cluster.index <- tmp[,"Cluster"]		
		names(cluster.index) <- tmp[,"File"]

		# Read summarised table
		file.names <- sample.annotation[file.list[[i]],"File"]
		result <- generate.summary.table(Result=table, FileName=file.names, FileFolder=summaries.dir, sampleInfo=sample.annotation[file.list[[i]],], PanelInfo=panel.info[panel.name,], Cluster=cluster.index, combine.type)
		
		tex <- tex.generator(cf, tex, SummaryTable=result, pdf.path=out.path, file.name=names(file.list[i]), cell.type=identifier,width=width, color.schema=color.schema,rotate=rotate, na.color=na.color)
		
		write.table(cbind(Stimulus=rownames(result),result),paste(table.out.dir,names(file.list[i]),".csv",sep=""),row.names=FALSE,col.names=TRUE,sep="\t")
	}

	tex <- c(tex.initial,tex)
	latex.write.main(cf, 'report', tex)

	return(0)        
}

main(execute)

