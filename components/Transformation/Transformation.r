library(componentSkeleton)
library(RColorBrewer)
library("geneplotter")

arcsinh <- function(x,c=arcsinh.cofactor) {
    f = log(x/c + sqrt((x/c)^2+1))
    return(f)

}

parDefault <- function(exp){
  vm <- data.frame(labelDescription=c(name="Name of Parameter",
                   desc="Description of Parameter",
                   range="Range of Parameter",
                   minRange="Minimum Parameter Value after Transformation",
                   maxRange="Maximum Parameter Value after Transformation"))

  pd <- data.frame(name=colnames(exp), desc=colnames(exp),
                   range=apply(exp, 2, max, na.rm=TRUE),
                   minRange=apply(exp, 2, min, na.rm=TRUE),
                   maxRange=apply(exp, 2, max, na.rm=TRUE))

  new("AnnotatedDataFrame", pd, vm)
}

validate.num.channels <- function(data, channels)
{
    max.channel <- max(channels)
    if(max.channel > ncol(data))
    {
        stop("Maximum channel number specified", max.channel, "is greater than number of channels in dataset", ncol(data))
    }
}

create.pairs.plot <- function(cf,index,path,plot.name, log, data, cofactor,width.inches,height.inches){

	n.column <- ncol(data)
	#jpeg(paste(path,"/",plot.name,".jpg",sep=""),width=width.inches*n.column, height=height.inches*n.column,quality=100)	
    png(paste(path,"/",plot.name,".png",sep=""),width=width.inches*n.column, height=height.inches*n.column)	
	#pdf(paste(path,"/",plot.name,".pdf",sep=""))
	par(mfrow=c(n.column,n.column),mar=c(1.5,1.5,1.5,1.5), oma=c(1,1,3,1))

    # Create a color map to indicate density.
    lab.palette <- colorRampPalette(brewer.pal(9, "PuBuGn"))

    # Number of outliers to plot
    n.outliers = floor(nrow(data) * 0.05)

	for(i in 1:n.column){
		y <- data[,i]
		for(j in 1:n.column){
			if(i==j){
				plot(x=c(1,2),y=c(1,2),type="n",xaxt="n",yaxt="n",xlab="",ylab="")
                # Calculate the right scaling for the variable names on the diagonal.
                label.widths <- strwidth(colnames(data), "user")
                cex.labels <- max(0.8, 1/max(label.widths))
				text(1.5,1.5,colnames(data)[i], cex=cex.labels)
			}else{
				x <- data[,j]
				smoothScatter(x,y,xlab="",ylab="", colramp=lab.palette, nrpoints=n.outliers)
			}
		}
	}
	mtext(log,side=3,outer=TRUE,cex=1.5)
	dev.off()
	
	caption <- paste("Transfromation visualization for output data file '",plot.name,"'"," with cofactor =", cofactor, ".", sep="")
    	tex <- latex.figure(paste(plot.name,".png",sep=""),
                        caption   = caption,
                        fig.label = sprintf('fig:%s_%d', get.metadata(cf, 'instanceName'), index), image.width=18, image.height=18)
    	return(tex)

}

execute <- function(cf) {
    # Read input
    input.facs.files <- get.input(cf,'fcsFiles')
    if (input.defined(cf, 'array')) {
        input.facs.array <- Array.read(cf, 'array')
    }
    # Read parameters
    channels <- get.parameter(cf,'channels','string')
    method <- get.parameter(cf,'transformationMethod','string')
    data.scale <- get.parameter(cf,'dataScale','string')
    width.inches  <- get.parameter(cf,  'width', 'float')
    height.inches <- get.parameter(cf, 'height', 'float')
    write.image <- get.parameter(cf,'writeImage','boolean')

    # Read component instance name.
    instance.name <- get.metadata(cf, 'instanceName')

    # Read output
    output.facs.files <- get.output(cf,'fcsResult')
    dir.create(output.facs.files)
    output.visual <- get.output(cf,'report')
    dir.create(output.visual)
    output.array <- get.output(cf,'array')
    dir.create(output.array)
    output.array.object <- Array.new()
    
    # Execute
    if(channels == "*")  {
        channels <- NULL
    }else {
        channels <- split.trim(channels, ",")
    }    

    #columns <- strsplit(channels,',')[[1]]

    #if(length(na.omit(columns)) != length(columns)){
    #	stop("Channels must be integer values!")
    #}

    dim = length(channels)

    suppressMessages(library(flowCore))
    if(input.defined(cf, 'fcsFiles')) {
        data.files <- list.files(input.facs.files)
        data.fullpath <- paste(input.facs.files, data.files,sep="/")
    } else {
        data.files <- c()
        data.fullpath <- c()
    }
    if(input.defined(cf, 'array')) {
        data.files<-c(data.files,input.facs.array[,'Key'])
        data.fullpath<-c(data.fullpath,input.facs.array[,'File'])
    }

    tex <- "\\subsection{Transformation visualization}"
    if (get.parameter(cf, 'pagebreak', 'boolean')) {
	    tex <- c('\\clearpage{}', tex)
    }


    if(method=="logicle"){

        m <- get.parameter(cf, 'LogicleM', 'float')
        if(m>10){
            stop("A logicle cofactor is usually greater than 0 and less than 10!")
        }
        if(data.scale=="18-bit"){
            TransObj <- logicleTransform("logicle", m=m, t=262144)
        }else if(data.scale=="4-decade"){
            TransObj <- logicleTransform("logicle", m=m, t=10000)
        }

        cofactor <- m
        main.log <- paste(": Logicle m = ",cofactor, sep="")


    }else if(method=="arcsinh"){
        a <- get.parameter(cf, 'ArcsinhA', 'float')
        b <- get.parameter(cf, 'ArcsinhB', 'float')
        c <- get.parameter(cf, 'ArcsinhC', 'float')

        TransObj <- arcsinhTransform(transformationId="ln-transformation", a=a, b=b, c=c)

        cofactor <- paste(a,b,c,sep=",")

        main.log <- paste(":Arcsinh a,b,c = ",cofactor, sep="") 

    }else if(method=="truncate"){
        a <- get.parameter(cf, 'TruncateA', 'float')

        TransObj <- truncateTransform(transformationId="Truncate-transformation", a=a)

        cofactor <- a

        main.log <- paste(":Truncate a = ",cofactor, sep="") 

    }else if(method=="scale"){

        a <- get.parameter(cf, 'ScaleA', 'float')
        b <- get.parameter(cf, 'ScaleB', 'float')

        TransObj <- scaleTransform(transformationId="Truncate-transformation", a=a, b=b)

        cofactor <- paste(a,b,sep=",")

        main.log <- paste(":Scale a,b = ",cofactor, sep="") 

    }else if(method=="linear"){

        a <- get.parameter(cf, 'LinearA', 'float')
        b <- get.parameter(cf, 'LinearB', 'float')

        TransObj <- linearTransform(transformationId="Linear-transformation", a=a, b=b)

        cofactor <- paste(a,b,sep=",")
        main.log <- paste(":Linear a,b = ",cofactor, sep="") 

    }else if(method=="quadratic"){

        a <- get.parameter(cf, 'QuadraticA', 'float')
        b <- get.parameter(cf, 'QuadraticB', 'float')
        c <- get.parameter(cf, 'QuadraticC', 'float')

        TransObj <- quadraticTransform(transformationId="Quadratic-transformation", a=a, b=b,c=c)

        cofactor <- paste(a,b,c,sep=",")
        main.log <- paste(":Quadratic a,b,c = ",cofactor, sep="") 

    }else if(method=="ln"){
	
        r <- get.parameter(cf, 'LnR', 'float')
        d <- get.parameter(cf, 'LnD', 'float')

        TransObj <- lnTransform(transformationId="ln-transformation", r=r, d=d)

        cofactor <- paste(r,d,sep=",")
        main.log <- paste(":Ln r,d = ",cofactor, sep="") 

    }else if(method=="log"){

        base <- get.parameter(cf, 'LogLogbase', 'float')
        r <- get.parameter(cf, 'LogR', 'float')
        d <- get.parameter(cf, 'LogD', 'float')

        TransObj <- logTransform(transformationId="log10-transformation", logbase=base, r=r, d=d)

        cofactor <- paste(base,r,d,sep=",")
        main.log <- paste(":Log base,r,d = ",cofactor, sep="") 
	
    }

    for(i in 1:length(data.files)){
        output.filename <- gsub( "\\.csv", "", data.files[i])
        input.filename <- data.fullpath[i]

        file <- CSV.read(input.filename)
        data<- as.matrix(file)

        if (is.null(channels)) {
            columns = which(colnames(data) %in% colnames(data))
        } else {
            columns = which(colnames(data) %in% channels)
        }
		subdata <- data[,columns]
		param <- parDefault(data)
        ff <- new("flowFrame",exprs=data,parameters=param)
		transform <- transformList(colnames(ff)[columns], TransObj@.Data) %on% ff
		transformed_data <- exprs(transform)

		# Read in the original column names to use as channel names.
        original.channel.names <- gsub("\"", "", unlist(strsplit(readLines(input.filename, n=1), "\t")))
    	colnames(transformed_data) <- original.channel.names

		transformed_data <- na.omit(transformed_data)
	
		write.table(transformed_data, file = paste(output.facs.files,"/",output.filename,".csv", sep = ""),sep = "\t", row.names = F, quote = F)
        output.array.object <- Array.add(output.array.object, 
                                      data.files[i],
                                      paste(output.facs.files,"/",output.filename,".csv", sep = ""))
        
		if(write.image){
		
			output.filename <- gsub("\\.","_",output.filename)
            output.filename <- paste(instance.name, output.filename, sep="-")
			
			main.log <- paste(output.filename,main.log,sep="")
		
			tex <- c(tex, create.pairs.plot(cf,i, output.visual,output.filename,main.log,transformed_data, cofactor, width.inches,height.inches))
		}
	}
    Array.write(cf, output.array.object, 'array')
	latex.write.main(cf, 'report', tex)

	return(0)
	
}

main(execute)
