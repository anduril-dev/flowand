source $ANDURIL_HOME/bash/functions.sh

clusterdir=$( getinput clusters ) 
populationsdir=$( getinput populations ) 

outputdir=$( getoutput filtered )
outarraydir=$( getoutput array )
outarrayidx=$( getoutput _index_array )

mkdir "$outputdir"
mkdir "$outarraydir"

popname=$( getparameter populationName )
clustcol=$( getparameter clusterColumn )

clusterfiles=( $( ls "$clusterdir") )
populationfiles=( $( ls "$populationsdir") )

if [ ${#clusterfiles[@]} -ne ${#populationfiles[@]} ];
then echo "Input folders contain different number of files!" >> "$errorfile"
     exit
fi

echo -e '"Key"'"\t"'"File"' > "${outarrayidx}"

time for (( f=0;f<${#clusterfiles[@]};f++ ))
do
    echo "Filtering file ${clusterfiles[$f]}"
    y=$( awk '/'"$popname"'/ {s=$3; gsub(/\"/,"",s); gsub(",","|",s); print s}' "$populationsdir"/"${populationfiles[$f]}" )
    head -n 1 "$clusterdir"/"${clusterfiles[$f]}" >> "$outputdir"/"${clusterfiles[$f]}"
    awk "NR==1 {for(i=1;i<=NF;i++) if (\$i ~ /$clustcol/) {f=i; break;}} NR>1 && \$f ~ /$y/" "$clusterdir"/"${clusterfiles[$f]}" >> "$outputdir"/"${clusterfiles[$f]}"
    key=$( echo ${clusterfiles[$f]} | sed 's,\.csv$,,i' )
    echo -e $key"\t"$outputdir"/"${clusterfiles[$f]} >> "${outarrayidx}"
done

