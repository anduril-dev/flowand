library(componentSkeleton)
library(flowCore)

# Keyword which identifies the compensation matrix in fcs-file.
COMPENSATION_MATRIX_KEYWORD = "SPILL"

# Column names for the annotations output CSV-files.
KEYWORD.COL.NAME <- "Keyword"
KEYWORD.VALUE.COL.NAME <- "Value"

execute <- function(cf) {
    # Get parameters, inputs and outputs
    fcsDir <- get.input(cf, 'fcsDir')
    keepOriginalChannelNames <- get.parameter(cf, 'keepOriginalChannelNames', 'boolean')
    expr.path <- get.output(cf, "expr")
    expr.raw.path <- get.output(cf, "exprRaw")
    annotations.path <- get.output(cf, "annotations")

    # Create exprArray output dir.
    expr.array.out.dir <- get.output(cf, 'exprArray')
    if (!file.exists(expr.array.out.dir)) {
        dir.create(expr.array.out.dir, recursive=TRUE)
    }

    # Create exprRawArray output dir.
    expr.raw.array.out.dir <- get.output(cf, 'exprRawArray')
    if (!file.exists(expr.raw.array.out.dir)) {
        dir.create(expr.raw.array.out.dir, recursive=TRUE)
    }

    # Create annotationsArray output dir.
    annotations.array.out.dir <- get.output(cf, 'annotationsArray')
    if (!file.exists(annotations.array.out.dir)) {
        dir.create(annotations.array.out.dir, recursive=TRUE)
    }

    file.names <- list.files(fcsDir, ".fcs$")
    dir.create(expr.path)
    dir.create(expr.raw.path)
    dir.create(annotations.path)

    # Array objects
    expr.array.output <- Array.new()
    expr.raw.array.output <- Array.new()
    annotations.array.output <- Array.new() 

    for (i in 1:length(t(file.names))) {
        # Take the filename without the ".fcs" suffix as the sample name.
        sample.name <- sub("\\.fcs", "", file.names[i])
    
        # Read in the data as a FlowFrame object.
        in.full.path <- paste(fcsDir, "/", t(file.names[i]), sep="")     
        ff <- read.FCS(in.full.path)
        
        # Apply compensation
        comp.matrix <- keyword(ff, COMPENSATION_MATRIX_KEYWORD)[[1]]
        
        if(is.null(comp.matrix)) {
            stop(sprintf("No spillover (compensation) matrix stored in file %s", filename))
        }
        ff.comp <- compensate(ff, comp.matrix)
        
        # Replace original channel names (antibodies) with the descriptions for the channels (proteins targeted by the antibodies).
        if(!keepOriginalChannelNames) {
            antibody.names <- ff@parameters@data$name
            target.names <- ff@parameters@data$desc
            names(target.names) <- antibody.names
            new.col.names <- colnames(ff@exprs)
            new.col.names[!is.na(target.names)] <- target.names[!is.na(target.names)]
            colnames(ff@exprs) <- new.col.names
            colnames(ff.comp@exprs) <- new.col.names
        }
    
        # Write single cell expression values for compensated data.
        csv.out <- cbind(Event=1:nrow(ff.comp@exprs),ff.comp@exprs)
        write.table(csv.out, paste(expr.path, "/", sample.name, ".csv", sep=""), sep="\t", row.names=FALSE)
        # Add expression matrix to array output.
        key = sample.name
        filename = paste(key, ".csv", sep="")
        CSV.write(paste(expr.array.out.dir, "/", filename, sep=""), csv.out)
        expr.array.output <- Array.add(expr.array.output, key, filename)

        # Write single cell expression values for uncompensated data.
        csv.out <- cbind(Event=1:nrow(ff@exprs),ff@exprs)
        write.table(csv.out, paste(expr.raw.path, "/", sample.name, ".csv", sep=""), sep="\t", row.names=FALSE)
        # Add raw expression matrix to array output.
        key = sample.name
        filename = paste(key, ".csv", sep="")
        CSV.write(paste(expr.raw.array.out.dir, "/", filename, sep=""), csv.out)
        expr.raw.array.output <- Array.add(expr.raw.array.output, key, filename)
    
        # Write annotation data for the FCS file.
        keywords <- keyword(ff)
        annotations <- matrix(ncol=2, nrow=length(keywords))
        colnames(annotations) <- c(KEYWORD.COL.NAME, KEYWORD.VALUE.COL.NAME)
        for(j in 1:length(keywords)) {
            value <- keywords[[j]]
            # Parse keyword value to a string to output.            
            if(class(value) == "character") {
                value <- value
            }else if(class(value) == "matrix") {
                temp <- file()
                write.table(value, temp, sep="\t", quote=FALSE)
                lines <- readLines(temp)
                value <- paste(lines, collapse="\n")
            }else {
                write.error(cf, sprintf("Class (%s) of value related to keyword '%s' not supported for output!", class(value), names(keywords)[j]))
            }
            annotations[j,] <- c(names(keywords)[j], value)
        }
        write.table(annotations, paste(annotations.path, "/", sample.name, ".csv", sep=""), sep="\t", row.names=FALSE)
        # Add annotations matrix to array output.
        key = sample.name
        filename = paste(key, ".csv", sep="")
        CSV.write(paste(annotations.array.out.dir, "/", filename, sep=""), annotations)
        annotations.array.output <- Array.add(annotations.array.output, key, filename)
    }

    ## Write array outputs
    Array.write(cf, expr.array.output, 'exprArray')
    Array.write(cf, expr.raw.array.output, 'exprRawArray')
    Array.write(cf, annotations.array.output, 'annotationsArray')

    return(0)
}

main(execute)
