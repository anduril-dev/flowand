#!/bin/bash


URL="http://csbi.ltdk.helsinki.fi/pub/anduril_static/component_data/facs/FCSReader.tgz"
cd "$( dirname $0 )"
[[ -d testcases/case1 ]] && {
    echo "Testcases already exist"
} || {
    mkdir -p testcases
    cd testcases
    wget -O - "$URL" | tar xzf - 
}

