library(componentSkeleton)
library(SamSPECTRAL)

execute <- function(cf) {
    # Get input directory.
    input.csv.list <- get.input(cf,'csvList')
    if (input.defined(cf, 'array')) {
        input.csv.array <- Array.read(cf, 'array')
    }
    # Get parameters and parse them.
	normal.sigma <- as.numeric(get.parameter(cf,'sigma', type="int"))
	separation.factor <- as.numeric(get.parameter(cf,'separationFactor', type="float"))

    channels.to.cluster <- get.parameter(cf, 'channelsToCluster')
    if(channels.to.cluster == "*")  {
        channels.to.cluster <- NULL
    }else {
        channels.to.cluster <- split.trim(channels.to.cluster, ",")
    }    
    cluster.id.col.name <- get.parameter(cf, 'clusterIDColName')

    # Get output directory paths and create them.
    output.clusters <- get.output(cf, 'clusters')
    dir.create(output.clusters)
    output.clusters.array <- get.output(cf, 'clustersArray')
    output.array.object <- Array.new()
    dir.create(output.clusters.array)
    
    ## Create task list (list of files to cluster).
    if(input.defined(cf, 'csvList')) {
        data.files <- as.list(list.files(input.csv.list, ".csv$"))
        data.fullpath <- as.list(paste(input.csv.list, data.files,sep="/"))
    } else {
        data.files <- c()
        data.fullpath <- c()
    }
    if(input.defined(cf, 'array')) {
        data.files<-as.list(c(data.files,input.csv.array[,'Key']))
        data.fullpath<-as.list(c(data.fullpath,input.csv.array[,'File']))
    }

    for(i in 1:length(data.files)){
        write.log(cf, sprintf("Clustering file: %s", data.files[i]))
        input.file.name <- file.path(data.fullpath[i])

        if (length(grep("\\.csv", data.files[i])) > 0) {
            csvstr <- ""
        } else {
            csvstr <- ".csv"
        }
        
		output.file.name <- file.path(output.clusters, paste(data.files[i], csvstr, sep=""))

        #output.file.name <- file.path(output.clusters, data.files[i])
        # Do the clustering.
        input.params <- list(input.file.name=input.file.name, 
                               channels.to.cluster=channels.to.cluster, 
                               output.file.name=output.file.name, 
                               normal.sigma=normal.sigma, 
                               separation.factor=separation.factor, 
                               cluster.id.col.name=cluster.id.col.name)
        clustering(cf,input.params)
        output.array.object <- Array.add(output.array.object, 
                                  data.files[[i]],
                                  output.file.name)
        print(sprintf("Finished clustering file: %s", output.file.name))
        write.log(cf, sprintf("Finished clustering file: %s", output.file.name))

    }
    
    Array.write(cf, output.array.object, 'clustersArray')
    
}

# Function to cluster the data (without RMPI)
clustering <- function(cf,input) {
    ## Cluster the data.
    csvOrig <- CSV.read(input$input.file.name)
    if (nrow(csvOrig) < 1) {
        CSV.write(input$output.file.name, csvOrig)
        return(0)
    }
    set.seed(38419)
    randnum <- sample(1:dim(csvOrig)[1])
    csv <- as.matrix(csvOrig[randnum,])
    channels.to.cluster <- input$channels.to.cluster[which(input$channels.to.cluster %in% colnames(csv))]
    data.matrix <- as.matrix(csv[, channels.to.cluster])

    m <- 3000
    community.weakness.threshold <- 1
    precision <- 6
    maximum.number.of.clusters <- 30
    space.length <- 3
    society <- Building_Communities(data.matrix, m, space.length, community.weakness.threshold)
    conductance <- Conductance_Calculation(data.matrix, input$normal.sigma, space.length, society, precision)
    clust_result <- Civilized_Spectral_Clustering(data.matrix, maximum.number.of.clusters, society, conductance, stabilizer = 1)

    labels.for_num.of.clusters <- clust_result@labels.for_num.of.clusters
    number.of.clusters <- clust_result@number.of.clusters
    component.of <- Connecting(data.matrix, society, conductance, number.of.clusters, labels.for_num.of.clusters, input$separation.factor)
    clust.res <- component.of$label

    res.csv <- cbind(csv, clust.res)
    if (input$cluster.id.col.name %in% colnames(res.csv)) {
        write.error(cf, sprintf('clusterIDColName %s is already present in the input file %s.', input$cluster.id.col.name, input$input.file.name))
        return(1)
    }
    colnames(res.csv) <- c(colnames(csv), input$cluster.id.col.name)

    res.csv <- as.data.frame(res.csv)
    #clustering produces NA clusters for some of the rows. It's not documented why!
    res.csv <- res.csv[!is.na(res.csv[,input$cluster.id.col.name]),]

    CSV.write(input$output.file.name, res.csv)
    return(0)
}

main(execute)
