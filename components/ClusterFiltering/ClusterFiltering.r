library(componentSkeleton)

# Function for filtering cells (lines) out of csv file.
filter <- function(d, filter.column, min.cell.number, filter.diff) {

	# Filter clusters with low number of cells
	data.to.keep <- as.data.frame(d[which(d[,"CellNumber"]>=min.cell.number),])
    flag <- rep(TRUE,nrow(data.to.keep))

    #for (i in 1:dim(data.to.keep)[1]) {
    #    if ((data.to.keep[i,"CD45"] < 1.0) & (data.to.keep[i,"SSC-A"] < 1.0))
    #        flag[i] <- FALSE
    #}

	scaled.centers <- scale(data.to.keep[,filter.column], center=TRUE, scale=FALSE)

	# Filter out clusters if median/mean of cluster is below average cluster mean/median
    # in all given columns
	flag <- rep(TRUE,nrow(data.to.keep))

    for(i in 1:dim(data.to.keep)[1]){
		tmp <- scaled.centers[i,]
        # reject cluster if all mean/medians below average
		if (sum(tmp<(-filter.diff)) == length(filter.column)) {
            flag[i] <- FALSE
		}
	}
	cluster.to.keep <- data.to.keep[flag,"Cluster"]
	return(cluster.to.keep)
}

# Function for combining cell intensities for each cluster.
combiner <- function(mlist, d, method, orig.column.names){
	res.list <- list()
	for(i in 1:length(mlist)){
		index <- mlist[[i]]
		sub.data <- d[index,]
		res.list[[i]] <- apply(sub.data,2,method)
		res.list[[i]] <- c(res.list[[i]],nrow(sub.data))
	}
	result <- do.call(rbind,res.list)
	result <- cbind(result,as.integer(names(mlist)))
	colnames(result) <- c(orig.column.names,"CellNumber","Cluster")
	return(result)
}

execute <- function(cf) {
	
	#library(sqldf)

	# Read input
	input.folder <- get.input(cf,'fcsDir')
	if (input.defined(cf, 'array')) {
        input.facs.array <- Array.read(cf, 'array')
    }
	
	# Read parameters
	filter.column <- get.parameter(cf,'filterColumn')
	filter.column <- split.trim(filter.column, ",")

	# Initiate output
	cluster.column <- get.parameter(cf, 'clusterColumn', 'string')
	min.cell.number <- get.parameter(cf, 'minCellNumber', 'int')
	method <- get.parameter(cf, 'combineMethod', 'string')
	filter.diff <- as.numeric(get.parameter(cf, 'filterDiff', 'float'))
	output.folder <- get.output(cf,'filtered')
	dir.create(output.folder)
	output.array <- get.output(cf,'array')
    dir.create(output.array)
    output.array.object <- Array.new()

	# Execute
	data.files <- list.files(input.folder)
	
	if(input.defined(cf, 'fcsDir')) {
        data.files <- list.files(input.folder)
        data.fullpath <- paste(input.folder, data.files,sep="/")
    } else {
        data.files <- c()
        data.fullpath <- c()
    }
    
    if(input.defined(cf, 'array')) {
        data.files<-c(data.files,input.facs.array[,'Key'])
        data.fullpath<-c(data.fullpath,input.facs.array[,'File'])
    }
    
    for(i in 1:length(data.files)){
	#for (file in data.files) {
		#write.log(cf,paste("Processing file: " , data.files[i],'\n',sep=""))
		input.filename <- data.fullpath[i]
        if (length(grep("\\.csv", data.files[i])) > 0) {
            csvstr <- ""
        } else {
            csvstr <- ".csv"
        }
        
		output.filename <- paste(output.folder, paste(data.files[i], csvstr, sep=""), sep="/")
		
		data <- CSV.read(input.filename)
		
		# Parse original column names.
		original.col.names <- gsub("\"", "", unlist(strsplit(readLines(input.filename, n=1), "\\t")))
		original.col.ids <- 1:length(original.col.names)

		# Check that filter column is present in file.
		if(filter.column[1]=="*"){
			flag <- original.col.names!=cluster.column
			filter.column.found <- original.col.ids[flag]
		} else {
			filter.column.found <- which(colnames(data) %in% filter.column)
			flag <- filter.column.found %in% original.col.ids
			if(length(flag[flag==FALSE])!=0){
				write.error(cf, sprintf('filterColumn %s is not found in input file %s', filter.column, file))
			}	
		}

		# Combine data for each cluster
		index <- which(original.col.names==cluster.column)
		tmp <- split(1:nrow(data),data[,index])
		combined.data <- as.data.frame(combiner(tmp, data[,-index], method,original.col.names[-index]))

		# Filter data.		
		clusters.to.keep <- filter(combined.data, filter.column.found, min.cell.number, filter.diff)

		#stm <- paste("SELECT * FROM data WHERE ",cluster.column," in ('",sep="")
		#stm <- paste(stm, paste(clusters.to.keep,collapse="','"),"')",sep="")
		#filtered.data <- sqldf(stm)
        filtered.data <- data[data[,cluster.column] %in% clusters.to.keep,]
		colnames(filtered.data) <- original.col.names
		
		write.table(filtered.data, output.filename, sep="\t",row.names=F)		
		output.array.object <- Array.add(output.array.object, 
										data.files[i],
										output.filename)
	}
	Array.write(cf, output.array.object, 'array')
	
	return(0)
}

main(execute)
