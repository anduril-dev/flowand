#### Integer/Integer-Linear Programming borrowed from the FLAME package
IP = function(dist.ST, cap.S, cap.T, delta, constraint) {

    nS = dim(dist.ST)[1]; nT = dim(dist.ST)[2]; n=nS+nT

    f.obj = as.vector(t(dist.ST))

    f.con.1 = f.con.3 = array(1:nS*nS*nT, dim=c(nS, nS*nT)) * 0
    f.con.2 = f.con.4 = array(1:nT*nS*nT, dim=c(nT, nS*nT)) * 0

    s=seq(1,nT)
    for (i in 1:nS) { f.con.1[i,s]=cap.T; f.con.3[i,s]=rep(1,nT); s=s+nT }

    t=seq(1,nS*nT,nT)
    for (i in 1:nT) { f.con.2[i,t]=cap.S; f.con.4[i,t]=rep(1,nS); t=t+1 }

    if (constraint=="11") {
        f.con = rbind(f.con.1,f.con.2,f.con.3,f.con.4)
        f.rhs = c(cap.S+delta, cap.T+delta, rep(1, n))
        f.dir = c(rep("<=", n), rep(">=", n))
        }  

    if (constraint=="10") {
        f.con = rbind(f.con.1,f.con.2,f.con.3)
        f.rhs = c(cap.S+delta, cap.T+delta, rep(1, nS))
        f.dir = c(rep("<=", n), rep(">=", nS))
        }  

    if (constraint=="01") {
        f.con = rbind(f.con.3, f.con.4)
        f.rhs = c(rep(1, nS),rep(1, nT))
        f.dir = rep(">=", n)
        }

    if (constraint=="00") {
        f.con = f.con.3
        f.rhs = rep(1, nS)
        f.dir = rep(">=", nS)
        }


    IP=lp ("min", f.obj, f.con, f.dir, f.rhs, all.bin=T)

    IP.solution=IP$solution

    return(matrix(IP$solution, nrow=nS, byrow=T))

}

####distanceSum function
#distanceSum<-function(x,y) { sqrt(sum((x-y)^2)) }

#### Execute function

library(componentSkeleton)

execute <- function(cf){

    # Call required libraries

    library(cluster)
    library(lpSolve)
    
    # Read input
    optClustInput <- get.input(cf,"optimalClustering")
    kMinPam <- get.parameter(cf,"kMinPam", "int")
    kMaxPam <- get.parameter(cf,"kMaxPam", "int")
    IPDelta <- get.parameter(cf,"IPDelta", "float")
    randomSeed <- get.parameter(cf,"randomSeed", "int")
    medianFactor <- get.parameter(cf,"medianFactor", "float")
    clusterCol <- get.parameter(cf,"clusterCol","string")
    metaChannels <- get.parameter(cf,"metaClusterChannels","string")

    outputDir=get.output(cf,"metaClusters")
    metaAssign=get.output(cf,"metaAssign")
    
    cat('test message')
    metaChannels <- gsub("^[ \t]+|[ \t]+$", "", metaChannels)
    metaChannels <- gsub(" *, *", ",", metaChannels)
    metaChannels<-unlist(strsplit(metaChannels, split=","))

    optClustFiles<-dir(optClustInput)
    set.seed(randomSeed)
    #Constructing meanMatrix
    tmpData=CSV.read(paste(optClustInput,optClustFiles[1],sep="/"))
        
    #Deal with the channels to include for metaCluster
    numeric.result <- as.numeric(metaChannels)
    if(length(which(is.na(numeric.result))) != 0){   
    #validate channel names

        mresult <- match(metaChannels, colnames(tmpData))

#Deal with the invalid column names, if there exist any     	
	    if (length(which(is.na(mresult)))>0){
		    write.error(cf, paste("Error",
		    ": \nCould not find the following channels in the dataset:\n "," ",
		    metaChannels[is.na(mresult)], "\n\n",sep=""))
				
		    return(PARAMETER_ERROR)
		}
		metaChannels=mresult
    } else {
	metaChannels=numeric.result
	max.metaChannels <- max(metaChannels)

        if(max.metaChannels > ncol(tmpData))
        {
		
		write.error(cf, paste("Error",
		": \nThe number of channels in dataset is", ncol(Data), 
		"but the maximum channel specified to cluster is", 
		max.metaChannels, "\n\n", 
		sep=""))
		return(PARAMETER_ERROR)
        }

    }
    
        #Finding the number of the column dedicated to the clusters
    if ( suppressWarnings(length(which(is.na(as.numeric(clusterCol)))))==0) clusterColNo=as.numeric(clusterCol) 
    if (suppressWarnings(length(which(is.na(as.numeric(clusterCol)))))!=0) clusterColNo=which(colnames(tmpData)==clusterCol) else clusterColNo=clusterCol  
    
    
    if (length(which(metaChannels==clusterColNo))!=1) {
    write.error(cf, 'Error! \nPlease include the "clusterCol" also in the "metaClusterChannels".')
    return(PARAMETER_ERROR)
    }
    
    metaChannels=metaChannels[-which(metaChannels==clusterColNo)]
    meanMatrix=matrix(0, nrow=0, ncol=(length(metaChannels)+2) )
    OriginalClustNumbers=c()
    colnames(meanMatrix)=c(paste("v",1:(ncol(meanMatrix)-2),sep=""), "cluster","prop")

    for (counter in 1:length(optClustFiles)){
        write.log(cf, paste("Reading", optClustFiles[counter], sep=" "))
        tmpData=CSV.read(paste(optClustInput,optClustFiles[counter],sep="/"))
        origTmpData=tmpData
        tmpData=tmpData[,metaChannels]
        OriginalClustNumbers=c(OriginalClustNumbers,origTmpData[,clusterColNo])
        tmpMeanMatrix=matrix( 0,nrow=length(unique(origTmpData[,clusterColNo])),
        ncol=(length(metaChannels)+2) )
        rownames(tmpMeanMatrix)=rep(optClustFiles[counter],nrow(tmpMeanMatrix))
        colnames(tmpMeanMatrix)=colnames(meanMatrix)
    
        tmpMeanMatrix[,(ncol(tmpMeanMatrix)-1)]= 1:length(unique(origTmpData[,clusterCol]))
        tmpMeanMatrix[,1:ncol(tmpData)]=apply(tmpData,2,function(temp1)tapply (as.vector(temp1),origTmpData[,clusterColNo],mean))
        tmpMeanMatrix=tmpMeanMatrix[order(tmpMeanMatrix[,(ncol(tmpMeanMatrix)-1)]), ]
    
        clusterFrequency=tapply(origTmpData[,clusterColNo], origTmpData[,clusterColNo], length) 
        clusterFrequnecy=clusterFrequency[order(as.numeric(names(clusterFrequency)))]
        tmpMeanMatrix[,ncol(tmpMeanMatrix)]=clusterFrequency/sum(clusterFrequency)
        
        meanMatrix=rbind(meanMatrix,tmpMeanMatrix)
    }

  
    # Calculating different Pam clusterings and choosing the 
    #one with the highest average silhouette width
    
    if(kMinPam==0) kMinPam= min(tapply(meanMatrix[,clusterCol],rownames(meanMatrix),max))
    if(kMaxPam==0) kMaxPam= max(tapply(meanMatrix[,clusterCol],rownames(meanMatrix),max))

    pamRes=sapply(kMinPam:kMaxPam,function(temp2)pam(meanMatrix[,-c(ncol(meanMatrix),(ncol(meanMatrix)-1))],temp2))
    
    colnames(pamRes)=kMinPam:kMaxPam
   
    avgSilhouteWidths=unlist(lapply(1:ncol(pamRes),function(temp3)pamRes["silinfo",][[temp3]]$avg.width))
    
    bestKPamIndex=which(avgSilhouteWidths==max(avgSilhouteWidths))
    bestKPam=(kMinPam:kMaxPam)[bestKPamIndex]
    meanMatrixClusters= unlist(pamRes["clustering",bestKPamIndex])
    meanMatrixMedoids=pamRes[["medoids",bestKPamIndex]]

    
    #Calculating medianProp (i.e median of the prop of the shortest included
    #optimal clsuters to the medoid of the Metaclusters) 
    #for each best Pam cluster.  
    
    k.NN = max(1,trunc(length(optClustFiles) * medianFactor))
    medianProp=c()
    for (i in 1:bestKPam){

        pick = which(meanMatrixClusters == i)
        prop.cluster = meanMatrix[pick,ncol(meanMatrix)]
        modes.cluster = meanMatrix[pick,-c(ncol(meanMatrix),(ncol(meanMatrix)-1))]

        mmPamDist=as.matrix(dist(rbind(modes.cluster, meanMatrixMedoids[i,]))) [length(pick)+1,1:length(pick)]

        dist.NN = sort(mmPamDist)[1:k.NN]
        indices.NN = which(mmPamDist %in% dist.NN)

        medianProp = c(medianProp,
        median(prop.cluster[indices.NN]))
    }
    
    
    #Starting the calculations of metaclusters
    #Beginning of Loop    
    metaClusters=c()
    for (counter in 1:length(optClustFiles)){
        tmpClustFile= CSV.read(paste(optClustInput,optClustFiles[counter],sep="/"))
        dataModes=meanMatrix[which(rownames(meanMatrix)==optClustFiles[counter]),-c(ncol(meanMatrix),(ncol(meanMatrix)-1))]
        
        distSt= as.matrix(dist(rbind(meanMatrixMedoids, dataModes)))[-(1:bestKPam),1:bestKPam]
        
        #Calling IP
        minObj = Inf
        for (constraint in c("11", "10", "01", "00")){
        IPRes=IP(distSt, cap.S=meanMatrix[which(rownames(meanMatrix)==optClustFiles[counter]),"prop"], cap.T=medianProp, delta=IPDelta, constraint)
            
        tryObj=sum(distSt*IPRes)
            if ((sum(is.na(IPRes))==0) & (tryObj<minObj) & tryObj!=0){
                optimalIPRes=  IPRes
                minObj=tryObj
            }
        }
        
        
        label=apply(optimalIPRes, 1, function(temp4)which(temp4==1))

        # Taking the lengthtmpClustFile of the label result
        lengthLabel=unlist(lapply(label, length))
        
        #In general the entries of "label" vector should be of length 1, 
        #and in the optimalIPRes (i.e. the linear optimization results) 
        #each real cluster of the data should be assigned to one of the
        # clusters form range [1,bestKPam] only. If not "label" will be a list
        # of vectors with at least one vector with a size > 1. the best choice
        # of those vectors should be chosen which are the nearest of those real 
        #clusters to the centre of the 1:bestKPam clusters.
        
        for (l.l in which(lengthLabel>1)) {

            distLabel= as.matrix(dist(rbind(meanMatrixMedoids, dataModes[unlist(label[[l.l]]),])))[-(1:bestKPam),1:bestKPam]
            sumDistLabel=rowSums(distLabel)
            distLabelOrder=order(sumDistLabel)
            label[[l.l]]=label[[l.l]][distLabelOrder[1]]
        }
        
        label=unlist(label)
       
        metaRes=matrix(0,ncol=2,nrow=length(label))
        colnames(metaRes)=c("fileCluster","metaCluster")
        metaRes[,2]=label
        metaRes[,1]=sort(unique(tmpClustFile[,clusterColNo]), decreasing=F)
        
        outputClustFile=tmpClustFile
        
        #Replace aligned clusyers numbers with original cluster numbers
        clusterIndex=rep(0,max(outputClustFile[,clusterColNo]))
        clusterIndex[sort(unique(outputClustFile[,clusterColNo]), decreasing=F)]=1:length(unique(outputClustFile[,clusterColNo]))
        outputClustFile[,clusterColNo]=clusterIndex[outputClustFile[,clusterColNo]]
        outputClustFile[,clusterColNo]=label[outputClustFile[,clusterColNo]]


        dir.create(outputDir)
        CSV.write(paste(outputDir, optClustFiles[counter],sep="/"),outputClustFile) 
        
        dir.create(metaAssign)
        write.log(cf, paste("Writing", optClustFiles[counter], "Metaclustering Results", sep=" "))
        CSV.write(paste(metaAssign, optClustFiles[counter],sep="/"), metaRes)         
    }      

}

main(execute)
