outputFiles <- unique(table1[,1])

outputDir <- get.output(cf,'optOut1')
dir.create(outputDir)

inc.cols <- unlist(strsplit(param1,","))
sub.cols <- gsub("[\\. ]","\\-",inc.cols)

colnames(table1) <- gsub("[\\. ]","-",colnames(table1))

for (i in 1:length(outputFiles)) {
    fileData <- table1[which(table1[,1]==outputFiles[i]),sub.cols]
    colnames(fileData) <- inc.cols
    dataMeans <- apply(fileData[,inc.cols[-1]],2,"mean")
    if (length(which(is.na(dataMeans))) > 0) {
        keep <- names(dataMeans[-which(is.na(dataMeans))])
        keep <- c(inc.cols[1], keep)
        fileData <- fileData[,keep]
    }
    CSV.write(paste(outputDir,outputFiles[i],sep="/"),fileData)
}

table.out <- data.frame()
rm(optOut1)
