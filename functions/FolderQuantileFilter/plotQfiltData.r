filtered.files <- list.files(var1, ".csv$")
original.files <- list.files(var2, ".csv$")

outputDir <- get.output(cf,'document')
#dir.create(outputDir)

tex <- "\\subsection{Quantile Filtering visualization}"
for (i in 1:length(filtered.files)) {
    filtered.name <- paste(var1,file.path(filtered.files[i]),sep="/")
    original.name <- paste(var2,file.path(original.files[i]),sep="/")
    plot.name <- paste(get.metadata(cf, 'instanceName'), strsplit(file.path(filtered.files[i]), "\\.csv")[[1]], sep="-")
    print(plot.name)
    data.filtered <- CSV.read(filtered.name)
    data.original <- CSV.read(original.name)
    data.original <- data.original[,colnames(data.filtered)]

    cols <- rep("gray", length(data.original[,1]))
    both <- data.original[which(data.original[,1] %in% data.filtered[,1]),1]
    cols[both] <- "black"
    n.column <- ncol(data.original)

    output.name <- paste(outputDir,"/",plot.name,".png", sep="")
    png(output.name, width=200*n.column, height=200*n.column)
	par(mfrow=c(n.column,n.column),mar=c(1.5,1.5,1.5,1.5), oma=c(1,1,3,1))
    pairs(data.original, pch=".", col=cols)
    dev.off()
	caption <- paste("Quantile filtered data for '",plot.name,"'",sep="")
    tmptex <- latex.figure(paste(plot.name,".png",sep=""),
                           caption   = caption,
                           fig.label = sprintf('fig:%s_%d', get.metadata(cf, 'instanceName'), i), image.width=18, image.height=18)

    tex <- c(tex, tmptex)
}

document.out <- tex
table.out <- data.frame()
